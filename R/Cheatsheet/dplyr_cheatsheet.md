# select
## Sélection des colonnes
### Sélectionne la colonne "col1"
	df %>% select(col1)
### Sélectionne les colonnes "col1" et "col2"
	df %>% select(col1, col2)
### Sélectionne toutes les colonnes sauf la colonne "col2"
	df %>% select(-col2)
### Sélectionne les colonnes allant de "col1" à "col5"
	df %>% select(col1:col5)
### Sélectionne toutes les colonnes sauf celles allant de "col1" à "col5"
	df %>% select(-(col1:col5))

# filter
## Sélection des lignes
### Sélectionne les lignes qui répondent à la condition: "col1" égal "val"
	df %>% filter(col1 == "val")
### Sélectionne les lignes qui répondent à la condition: "col1" égal "val" ET "col2" supérieur à 0
	df %>% filter(col1 == "val", col2 > 0)
### Sélectionne les lignes qui répondent à la condition: "col1" égal "val" OU "col2" supérieur à 0
	df %>% filter(col1 == "val" | col2 > 0)

# arrange
## Tri des lignes
### Tri les lignes de df en fonction de col1 (ascendant)
	df %>% arrange(col1)
### Tri les lignes de df en fonction de col1 (descendant)
	df %>% arrange(desc(col1))
### Tri les lignes de df en fonction de col1 (ascendant) puis de col2 (descendant)
	df %>% arrange(col1, desc(col2))

# mutate
## Ajout de colonnes
### Ajoute une nouvelle colonne en fonction des valeurs de col1 et col2
	df %>% mutate(col3 = col1 + col2)
### Ajoute une nouvelle colonne en fonction des valeurs d'une autre nouvelle colonne
	df %>% mutate(col3 = col1 + col2, col4 = col3 / 2)

# summarize
## Syntèse de variables
### Groupe les valeurs similaires de col1 en n groupes et renvoie un tableau de n ligne (pour chaque groupe) avec une colonne qui contient les valeurs groupé de "col1", "count" le nombre de valeur pour ce groupe, "unique" le nombre de valeur unique dans "col2" pour ce groupe et "avg" la moyenne des valeurs de "col3" pour ce groupe
	df %>% group_by(col1) %>% sumarize(count = n(), unique = n_distinct(col2), avg = mean(col3))
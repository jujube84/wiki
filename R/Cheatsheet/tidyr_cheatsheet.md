# gather
##	Rassemble toutes les colonnes sauf "grade" en une colonne "sex", les valeurs de ces colonnes seront mise dans la colonne "count" 
	students %>% gather(key = sex, value = count, -grade)

# separate
## Sépare une colonne qui contient 2 variables ("sex" et "class") en deux nouvelles colonnes
	res %>% separate(col = sex_class, into = c("sex", "class"))

# spread
## Ajoute deux colonnes correspondantes aux valeurs de "test" en se répartisant les valeurs de "grade"
	student %>% spread(key = test, value = grade)

# Get the current date
    today() # 2019-11-05 (Date)

# Get the current date-time
    now() # 2019-11-05 10:55:10 (POSIXct)

# Parsing
## Parse a string representing a date
    ymd("1989 May 17") # 1989-05-17
    mdy("March 12, 1975") # 1975-03-12
## Parse a number representing a date
    dmy(25081985) # 1985-08-25
## Parse a string representing a date-time
    ymd_hms("2014-08-23 17:23:02") # 2014-08-23 17:23:02 UTC (POSIXct, POSIXt)
## Parse a string representing a time
    hms("03:22:14") # 3H 22M 14S (lubridate::Period)
## Parse a vector of strings representing a date
    ymd(c("2014-05-14", "2014-09-22", "2014-07-11")) # 2014-05-14 2014-09-22 2014-07-11
## Parse a string representing a date using a different timezone
    mdy("June 17, 2008", tz = "Singapore") # 2008-06-17 +08 (POSIXct POSIXt)

# update
## Modify a date
    update(today(), day = 1, month = 8) # 2019-08-01
## Modify a date-time
    update(today(), hours = 8, minutes = 34, seconds = 55) # 2019-11-05 08:34:55 CET
## Add a period
    today() + days(2) # 2019-11-07
    today() + months(2) # 2020-01-05
    now() + hours(2) + minutes(8) # 2019-11-05 13:49:49 CET

# Get date-time for a different timezone
    now(tzone = "America/New_York") # 2019-11-05 08:57:14 EST
    with_tz(now(), tzone = "Asia/Hong_Kong") # 2019-11-08 22:24:20 HKT
    
# interval
## Find a period between 2 date
    as.period(interval(start = today(), end = today() + days(2))) # 2d 0H 0M 0S
## Find a period between 2 time
    as.period(interval(start = now(), end = now() + hours(2))) # 2H 0M 0.000458002090454102S

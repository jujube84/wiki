#include <vector>

struct BPoints
{
  BPoints(int* b, std::size_t sz) : block_(b), size_(sz) {}

  int* field(std::size_t i) { return &block_[i*size_]; }

  private:
  int* block_;
  std::size_t size_;
};

struct AoPoints
{
  AoPoints(std::size_t nblock, std::size_t sz) : data(nblock*2*sz), size_(sz) {}

  BPoints block(std::size_t i) { return {&data[i*size_*2], size_}; }

  private:
  std::vector<int> data;
  std::size_t size_;
};

int main(int argc, char**)
{
  AoPoints p(argc,128);

  for(std::size_t i=0; i<argc;++i)
  {
    auto xs = p.block(i).field(0);
    auto ys = p.block(i).field(1);

    for(std::size_t n=0;n<128;++n)
      xs[n] = 2*xs[n] + ys[n];
  }
}

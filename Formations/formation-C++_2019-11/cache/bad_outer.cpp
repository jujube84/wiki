#include <iostream>
#include <chrono>
#include "matrix.hpp"

matrix outer_sum(matrix const& mat)
{
  matrix vec( mat.width(),1 );

  for(std::size_t i=0;i!=mat.width();++i)
  {
    double result = 0.;

    for(std::size_t j=0;j!=mat.height();++j)
      result += mat(i,j);

    vec(i,0) = result;
  }

  return vec;
}

matrix res;

int main(int argc, char** argv)
{
  std::vector<std::string> args(argv+1,argv+argc);

  std::size_t n = std::stoi(args[0]);
  std::size_t r = std::stoi(args[1]);

  matrix mat(n,n);

  for(std::size_t i=0;i!=mat.height();++i)
  {
    for(std::size_t j=0;j!=mat.width();++j)
      mat(j,i) = (j+1) + 1000*(i+1);
  }

 //std::cout << mat << "\n";


  auto start = std::chrono::steady_clock::now();
  for(int i=0;i<r;++i)
    res = outer_sum(mat);
  auto stop = std::chrono::steady_clock::now();

  auto timing = stop - start;

  std::cout << (2.2*std::chrono::duration<double,std::nano>(timing).count())/(r*n);

 //std::cout << v << "\n";

 return 0;
}

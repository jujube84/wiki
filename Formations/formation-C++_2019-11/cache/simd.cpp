#include <xmmintrin.h>
#include <vector>
#include <iostream>

template<typename T> struct register_of
{
  typedef __m128i type;
};

template<> struct register_of<float>
{
  typedef __m128 type;
};

template<> struct register_of<double>
{
  typedef __m128d type;
};

template<typename T> struct pack
{
  typedef typename register_of<T>::type storage_type;
  static const std::size_t cardinal = sizeof(storage_type)/sizeof(T);

  storage_type data_;
};

__m128 load(float const* p)
{
  return _mm_loadu_ps(p);
}

void store(__m128 v, float* p)
{
  return _mm_storeu_ps(p,v);
}

__m128 add(__m128 a, __m128 b)
{
  return _mm_add_ps(a,b);
}

__m128 mul(__m128 a, __m128 b)
{
  return _mm_mul_ps(a,b);
}

int main()
{
  std::vector<float> a(1024,1.f), b(1024,1.f);
  float out[4];
a[125] = 95.6f;
b[980] = 889.7f;

  __m128 acc = _mm_setzero_ps();

  for(int i=0;i<1024;i+=pack<float>::cardinal)
  {
    __m128 va = load(&a[i]);
    __m128 vb = load(&b[i]);

    acc = add(acc,mul(va,vb));
  }

  store(acc, &out[0]);
  for(int i=0;i<pack<float>::cardinal;++i)
    std::cout << out[i] << " ";
  std::cout << "\n";

  auto acc2 = _mm_shuffle_ps(acc,acc,0b01001110 );
  acc  = add(acc,acc2);
  acc2 = _mm_shuffle_ps(acc,acc,0b00011011);
  acc  = add(acc,acc2);

  store(acc, &out[0]);
  for(int i=0;i<pack<float>::cardinal;++i)
    std::cout << out[i] << " ";
  std::cout << "\n";
}

enum class fields { x_, y_ };

struct BPoints
{
  BPoints(int* b, int sz) : block_(b), size_(sz) {}

  int* field(int i) const { return &block_[i*sz]; }

  int* x() const { return field(0); }
  int* y() const { return field(1); }

  private:
  int* block_;
  int size_;
};

template<int FieldSize>
struct AoPoints
{
  AoPoints(int nblock, int sz)
       : data(nblock*FieldSize*sz), size_(sz) {}

  BPoints block(int i) const
  {
    return {&data[i*size_*FieldSize], size_};
  }

  int * xs(int i) { return block(i).x();}
  int * ys(int i) { return block(i).y(); }

  private:
  std::vector<int> data;
  int size_;
};


int main()
{
  AoPoints<2> p(16,128);

  for(int i=0; i<16;++i)
  {
    auto& xs = p.xs(i);
    auto& ys = p.ys(i);

    for(int n=0;n<128;++n)
      xs[n] = 2*xs[n] + ys[n];
  }
}

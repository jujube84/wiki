#ifndef MATRIX_HPP_INCLUDED
#define MATRIX_HPP_INCLUDED

#include <iostream>
#include <vector>

struct matrix
{
  using value_type = double;
  public:

  matrix(std::size_t w = 0, std::size_t h = 0)
        : data_(h*w),height_(h),width_(w) {}

  value_type& operator()(std::size_t col, std::size_t row)
  {
    return data_[col + row*width_];
  }

  value_type operator()(std::size_t col, std::size_t row) const
  {
    return data_[col + row*width_];
  }

  std::size_t width() const   { return width_;  }
  std::size_t height() const  { return height_; }

  private:
  std::vector<value_type> data_;
  std::size_t height_, width_;
};

std::ostream& operator<<(std::ostream& os, matrix const& mat)
{
  for(std::size_t i=0;i!=mat.height();++i)
  {
    for(std::size_t j=0;j!=mat.width();++j)
      std::cout << mat(j,i) << " ";
    std::cout << "\n";
  }
  return os;
}

#endif

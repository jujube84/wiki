struct Point
{
  int x;
  int y;
};

// Array of Structures
struct Ball
{
  Point position;
  Point velocity;
  std::string name;
};

// Stockage des donnees
std::vector<Ball> balls;

// Acces complet
void update_positions(std::vector<Ball>& bs)
{
  for(auto& b : bs)
  {
    b.position.x += b.velocity.x;
    b.position.y += b.velocity.y;
  }
}

// Acces partiel
void print_names(const Ball& b)
{
  std::cout << b.name << '\n';
}

// Structure of Arrays approach

// Le stockage est interne a la structure
struct Balls
{
  int n_balls;
  std::vector<int> position_x;
  std::vector<int> position_y;
  std::vector<int> velocity_x;
  std::vector<int> velocity_y;
  std::vector<std::string> names;
};

// Acces total : connaissance des champs
void update_positions(Balls& bs)
{
/*
  for(auto& b : bs)
  {
    b.position.x += b.velocity.x;
    b.position.y += b.velocity.y;
  }

*/
  for(int i = 0; i < bs.n_balls; ++i)
  {
    bs.position_x[i] += bs.velocity_x[i];
    bs.position_y[i] += bs.velocity_y[i];
  }
};

// Acces partiel
void print_name(const Balls& bs, int i)
{
  std::cout << bs.names[i] << '\n';
}

#include <iostream>
#include <chrono>
#include <vector>
#include <string>
#include <cstdint>

template<typename T>
struct matrix
{
  public:

  matrix(std::size_t w = 0, std::size_t h = 0) : data_(h*w),height_(h),width_(w) {}

  T& operator()(std::size_t col, std::size_t row)
  {
    return data_[col + row*width_];
  }

  T operator()(std::size_t col, std::size_t row) const
  {
    return data_[col + row*width_];
  }

  std::size_t width() const   { return width_;  }
  std::size_t height() const  { return height_; }

  private:
  std::vector<T> data_;
  std::size_t height_, width_;
};

template<typename T>
void filter ( matrix<T> const& m, matrix<T>& r
            , std::size_t i0, std::size_t i1
            , std::size_t j0, std::size_t j1
            )

{
  for(std::size_t j = j0; j<j1; ++j)
    for(std::size_t i = i0; i<i1; ++i)
    {
      float f =   m(i-1,j-1) + 2*m(i,j-1) +   m(i+1,j-1)
              + 2*m(i-1,j  ) + 4*m(i,j  ) + 2*m(i+1,j )
              +   m(i-1,j+1) + 2*m(i,j+1) +   m(i+1,j+1);
      r(i,j)  = f;
    }
}

template<typename T>
matrix<T> filter( matrix<T> const& m, std::size_t block_size )
{
  std::size_t w = m.width();
  std::size_t h = m.height();

  std::size_t nbw = (w+block_size-1)/block_size;
  std::size_t nbh = (h+block_size-1)/block_size;

  matrix<T> r(w,h);

  std::size_t j0 = 1;
  for(std::size_t bj=0;bj<nbh;++bj)
  {
    std::size_t j1 = std::min<std::size_t>(j0 + block_size,h-1);
    std::size_t i0 = 1;
    for(std::size_t bi=0;bi<nbw;++bi)
    {
      std::size_t i1 = std::min<std::size_t>(i0 + block_size,w-1);
      filter(m,r, i0,i1,j0,j1);
      i0 = i1;
    }
    j0 = j1;
  }
  return r;
}


template<typename T>
matrix<T> filter( matrix<T> const& m )
{
  std::size_t w = m.width();
  std::size_t h = m.height();
  matrix<T> r(w,h);
  filter(m,r, 1,w-1, 1,h-1);
  return r;
}

int main(int argc, char** argv)
{
  std::vector<std::string> args(argv+1,argv+argc);

  std::size_t maxn = std::stoi(args[0]);
  std::size_t r = std::stoi(args[1]);
  std::size_t b = std::stoi(args[2]);

  for(int n = 32; n < maxn; n += 32)
  {
    matrix<std::uint32_t> mat(n,n), res;

    for(std::size_t j=0;j<mat.height();++j)
    {
      for(std::size_t i=0;i<mat.width();++i)
        mat(i,j) = j%2 ? (i%2 == 0) : (i%2 != 0);
    }

    auto start = std::chrono::steady_clock::now();
    for(int i=0;i<r;++i) res = filter(mat,b);
    auto stop = std::chrono::steady_clock::now();

    auto timing = stop - start;
    auto t = std::chrono::duration<double,std::micro>(timing).count()/r;

    std::cout << n << " elmt "
              << (n*n*sizeof(res(0,0)))/1024. << " Kbytes "
              << t << " us "
              << t/(n*n) << " us/elemt "
              << (2.5e9*t*1e-6)/(n*n) << " cycle/elemt\n";
  }
}

#include <omp.h>
#include <iostream>
#include <vector>
#include <chrono>
#include <math.h>

/* void print_threads() {
    int p = 3;

    #pragma omp parallel
    {
        printf("OMP %d/%d = %d\n", omp_get_thread_num() + 1,
                                   omp_get_num_threads(),
                                   p);
    }

    std::cout << p << "\n";
} */

void print_double_vector(std::vector<double> const& v){
    for(double const& e : v){
        std::cout << e << " ";
    }
    std::cout << "\n";
}

int main(int argc, char* argv[]) {
    size_t size = std::atoi(argv[1]);
    size_t r = std::atoi(argv[2]);
    std::vector<double> d(size, 1);
    std::vector<double> e(size, 1);
    std::vector<double> v(size);

    /* auto start = std::chrono::steady_clock::now();
    #pragma omp parallel for
    for(size_t rep = 0; rep < r; ++rep) {
        for(size_t i = 0; i < size; ++i){
            d[i] = std::rand() / double(RAND_MAX);
            e[i] = std::rand() / double(RAND_MAX);
        }
    }
    auto stop = std::chrono::steady_clock::now();
    auto time = stop - start;
    std::cout << "rand: " << std::chrono::duration<double, std::micro>(time).count()/r << "ms\n"; */
    
    auto start = std::chrono::steady_clock::now();
    for(size_t rep = 0; rep < r; ++rep){
        #pragma omp parallel for
        for(int i = 0; i < size; ++i){
            v[i] = std::atan(std::sqrt(std::cos(1. /*/ (d[i] + e[i])*/)));
        }
    }
    auto stop = std::chrono::steady_clock::now();
    auto time = stop - start;
    std::cout << "mean: " << std::chrono::duration<double, std::micro>(time).count()/r << "ms\n";

    std::cout << "cycle/elem: " << (3.2*std::chrono::duration<double, std::nano>(time).count()/r)/size << "\n";

    start = std::chrono::steady_clock::now();
    double res = 0.;
    # pragma omp parallel for reduction(+:res)
    for(std::size_t i=0; i < size; ++i) {
        v[i] = 10 * v[i];
        res += v[i];
    }
    stop = std::chrono::steady_clock::now();
    time = stop - start;
    std::cout << "mean: " << std::chrono::duration<double, std::micro>(time).count()/r << "ms\n";
    std::cout << res << "\n";
}
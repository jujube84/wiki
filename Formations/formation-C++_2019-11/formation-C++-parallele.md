# Formation C++ Calculs parallèles
# Jour 1
# Intro
## Page 1
Objectifs
- Découverte d'outils
- Pièges à éviter

## Page 2
3 Niveaux d'optimisation

Toujours commencer par l'optimisation de haut puis moyen et enfin bas niveau. Ici nous passons directement au bas niveau

## Page 3
Faire confiance au compilateur

# Notions de Base
## Page 4
- Métrique mémoire: octets lu par seconde
- Métrique calculs: FLOPS mais moins bon que cycle/élément (élément = pixel par ex, valeur du métier)

## Page 6
Roofline Model: Borne dû au limitation de la bande passante mémoire

## Page 8
Toujour sortir de la zone memory bound avant pour optimiser la paralellisation

## Page 9
Plus difficile de traiter des problèmes O(1) ou O(log(N)) que O(N)

## Page 11
Concevoir des algo sans états partagés (attributs d'objets qui vont être accéder par un autre), Nécéssite des mutex

# Boucles et Layout mémoire
## Page 15
- L1 : cache très rapide mais très petit
- L2 : cache moins rapide, un peu plus gros
- L3 : cache très gros partagé entre les coeurs


## Page 20
Importance de vérifier que le code fonctionne bien avec le cache (si possible faire rentrer toutes les données du code dans le cache L1 car évite d'avoir des problèmes)

## Page 23
- Vérifier le sens des boucles pour éviter les caches miss
- Notion de col et de ligne = logique, pour la machine -> que des octets contigus

# Ex1
- Pour le benchmark: prendre la médianne plutot que la moyenne et la val min/max
- Voir framework googlebenchmark
- Compile flag: -ffast-math -> optimise le calcul mais attention aux effets de bords

## Page 23
- Fusion de boucle: Voir refactoring Landsepi
- Attention si les valeurs calculés dépendent de valeurs futures
- Pas de if sur les index dans les boucles pour gerer les cas particuliers d'index

## Page 26
Blocking: Faire des sous-tableaux qui rentrent dans les caches

## Page 27
- AoSoA -> `std::vector< objects_block > objects(n);` avec `object_block` une structure de tableau de valeur
- On choisit si on veut le coté SoA ou AoS avec le constructeur ou on peut faire un peu des deux
- Maximise la contiguité, la localisation spatial et temporelle

# Apparté
- Utiliser -O3 pour que le compilateur cherche à vectoriser tout seul (de manière général, utiliser -O3 plutot que -O2, sauf cas particulier)
- Intel Intrinsics Guide: fonctions C pour acceder directement à des instructions Intel

# Métrique pour la performance

# Jour 2
# Programmation parallèle avec OpenMP
## Page 45
- Pour compiler du code avec OpenMP : `g++ x.cpp -o x -fopenmp`
- Eviter les variables mutables partagés au maximum (pour éviter de payer le coût des mutex)

## Page 48
`nowait` -> les threads continuent sur la tache suivante une fois qu'il a fini son travail, quand il y a deux boucles indépendantes qui se suivent par exemple

## Page 50
`schedule(static)` utilisé par défaut

## Page 54
- Normalement, une ligne de cache = 64 octets
- Intel parallel studio pour analyse de perf (monitoring du refresh du cache / rooftop)

## Ex2
`OMP_NUM_THREADS=X ./a.out 1000 1000`

# Jour 3
# Programmation parallèle avec MPI
## Page 82
- MPI_Comm_size: Nombre de PE (num_threads)
- MPI_Comm_rank: Numéro du PE (thread_num)

## Page 87
Hostfile: `$IP slots=x`

## Page 99
- Utiliser MPI puis openMP, jamais l'inverse
- Envoyer les messages en dehors des threads openMP

# Ex5
Ne pas hésiter à faire des wrappers pour les fonctions MPI pour faciliter leur utilisation

# Conclusion
- Toujours aller le long du cache pour éviter de charger des données inutilisées
- `-O3`: Vectorise les calcules si bonnes conditions + `-fastmath` pour aller plus vite (Attention aux risques)
- OpenMP: Coût de mise en place quand parallélisation de données proche de 0 (pragma omp parallel for et terminé)
- Penser en many to few (au cas avec beaucoup de données plutôt que l'inverse)
- Ne pas forcement partir avec la version séquentielle car pas forcement adapté à la parallèlisation
- MPI et équivalent: Priviliger les opérations collectives à celles point-à-point
- Bien faire ses benchmarks (accélération vs efficacité), comparer la meilleur version séquentielle (pas la version parallèle avec nb_thread=1) avec une version parallèle
- Ne pas aller trop loin dans l'optimisation sauf si vraiment besoin (peut être contre-productif)

gmail.com
#include <mpi.h>
#include <iostream>

enum status {
    OK = 0,
    STOP = 1
};

void pong(int& k, int const& max, int const& target, int const& sink) {
    MPI_Recv(&k, 1, MPI_INT, target, OK, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    if(k < max) {
        k++;
        MPI_Send(&k, 1, MPI_INT, sink, OK, MPI_COMM_WORLD);
    }
    else {
        MPI_Send(&k, 1, MPI_INT, sink, STOP, MPI_COMM_WORLD);
    }
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int max = std::atoi(argv[1]);
    int k = 0;
    int start_rank = 0;
    int target = (rank + 1) % size;
    int sink = (rank + size - 1) % size;

    if(rank == start_rank) {
        std::cout << "start=" << rank << " / k=" << k << "\n";
        MPI_Send(&k, 1, MPI_INT, target, OK, MPI_COMM_WORLD);
    }

    if(rank < size) {
        while (k < max) {
            pong(k, max, target, sink);
        }
    }

    if(rank == (max + start_rank) % 2) {
        int final;
        MPI_Recv(&final, 1, MPI_INT, sink, STOP, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        std::cout << "final=" << final << "\n";
    }

    MPI_Finalize();
}
#include <mpi.h>
#include <iostream>
#include <vector>

void print_vector(std::vector<float> const& v){
    for(float const& e : v){
        std::cout << e << " ";
    }
    std::cout << "\n";
}

MPI_Datatype mpi_datatype(int) { return MPI_INT; }
MPI_Datatype mpi_datatype(float) { return MPI_FLOAT; }
//...

template<typename T>
std::vector<T> parallel_sum(std::vector<T> const& d){
    std::vector<T> out(d.size());
    if(d.size()) {
        MPI_Reduce(d.data(), out.data(), d.size(), mpi_datatype(d[0]), MPI_SUM,
                   0, MPI_COMM_WORLD);
    }

    return out;
}

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);

    srand(time(nullptr));
    int size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    size_t k = 4;
    size_t nb_elem = k * size;
    std::vector<float> in;
    std::vector<float> buffer(k);
    std::vector<float> out(k);
    int count;

    if(rank == 0) {
        std::cout << "nb_elem = " << nb_elem << "\n"; 
        in.resize(nb_elem);
        for (auto& e : in) {
            e = (rand() / float(RAND_MAX)) * 200.f - 100;
        }
        print_vector(in);
    }
    else {
        in.resize(k);
    }

    MPI_Scatter(in.data(), k, MPI_FLOAT, 
                buffer.data(), k, MPI_FLOAT, 
                0, MPI_COMM_WORLD);

    for(size_t i = 0; i < k; i++) {
        out[i] = buffer[i] < 0 ? 0 : 1;
        std::cout << " out[" << i << "] = " << out[i] << "\n";
    }

    int res = 0;
    for(auto const& e : out) {
        res += e;
    }

    MPI_Reduce(&res, &count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if(rank == 0) {
        std::cout << "count = " << count << "\n";
    }

    MPI_Finalize();
}
#include "matrix.hpp"
#include <omp.h>
#include <chrono>

struct box {
    int i0, i1, j0, j1;
};

void filter_seq(matrix const& in, matrix& out, int const& radius) {
    float area = (2*radius+1)^2;
    for (size_t i = radius; i < in.height()-radius; ++i){
        for (size_t j = radius; j < in.width()-radius; ++j){
            for (int u = -radius; u <= radius; ++u){
                for (int v = -radius; v <= radius; ++v){
                    out(j, i) += in(j+v, i+u);
                }
            }
            out(j, i) /= area;
        }
    }
}

void filter_par_height(matrix const& in, matrix& out, int const& radius) {
    float area = (2*radius+1)^2;
    #pragma omp parallel for
    for (size_t i = radius; i < in.height()-radius; ++i){
        for (size_t j = radius; j < in.width()-radius; ++j){
            for (int u = -radius; u <= radius; ++u){
                for (int v = -radius; v <= radius; ++v){
                    out(j, i) += in(j+v, i+u);
                }
            }
            out(j, i) /= area;
        }
    }
}

void filter_par_width(matrix const& in, matrix& out, int const& radius) {
    float area = (2*radius+1)^2;
    for (size_t i = radius; i < in.height()-radius; ++i){
        #pragma omp parallel for
        for (size_t j = radius; j < in.width()-radius; ++j){
            for (int u = -radius; u <= radius; ++u){
                for (int v = -radius; v <= radius; ++v){
                    out(j, i) += in(j+v, i+u);
                }
            }
            out(j, i) /= area;
        }
    }
}

void filter_par_height_width(matrix const& in, matrix& out, int const& radius) {
    float area = (2*radius+1)*(2*radius+1);
    #pragma omp parallel for
    for (size_t i = radius; i < in.height()-radius; ++i){
        #pragma omp parallel for
        for (size_t j = radius; j < in.width()-radius; ++j){
            for (int u = -radius; u <= radius; ++u){
                for (int v = -radius; v <= radius; ++v){
                    out(j, i) += in(j+v, i+u);
                }
            }
            out(j, i) /= area;
        }
    }
}

int main(int argc, char* argv[]) {
    size_t w = std::atoi(argv[1]);
    size_t h = std::atoi(argv[2]);
    size_t r = std::atoi(argv[3]);
    size_t radius = std::atoi(argv[4]);

    matrix in = matrix(w, h);
    matrix out = matrix(w, h);

    for(int i=0; i < in.width(); i++) {
        for(int j=0; j < in.height(); j++) {
            in(j, i) = 1;
        }
    }

    auto start = std::chrono::steady_clock::now();
    for(size_t rep = 0; rep < r; ++rep){
        filter_seq(in, out, radius);
    }
    auto stop = std::chrono::steady_clock::now();
    auto time = stop - start;
    std::cout << "seq cycle/elem: " << (3.2*std::chrono::duration<double, std::nano>(time).count()/r)/(w*h) << "\n";

    start = std::chrono::steady_clock::now();
    for(size_t rep = 0; rep < r; ++rep){
        filter_par_height(in, out, radius);
    }
    stop = std::chrono::steady_clock::now();
    time = stop - start;
    std::cout << "par height cycle/elem: " << (3.2*std::chrono::duration<double, std::nano>(time).count()/r)/(w*h) << "\n";

    start = std::chrono::steady_clock::now();
    for(size_t rep = 0; rep < r; ++rep){
        filter_par_width(in, out, radius);
    }
    stop = std::chrono::steady_clock::now();
    time = stop - start;
    std::cout << "par width cycle/elem: " << (3.2*std::chrono::duration<double, std::nano>(time).count()/r)/(w*h) << "\n";

    start = std::chrono::steady_clock::now();
    for(size_t rep = 0; rep < r; ++rep){
        filter_par_height_width(in, out, radius);
    }
    stop = std::chrono::steady_clock::now();
    time = stop - start;
    std::cout << "par height width cycle/elem: " << (3.2*std::chrono::duration<double, std::nano>(time).count()/r)/(w*h) << "\n";
}
#if defined(_OPENMP)
#include <omp.h>
#endif

#include <iostream>
#include <cstdio>

int get_thread_num()
{
#if defined(_OPENMP)
  return omp_get_thread_num();
#else
  return 0;
#endif
}

int get_num_threads()
{
#if defined(_OPENMP)
  return omp_get_num_threads();
#else
  return 1;
#endif
}

int main()
{

  int p = 3;
  #pragma omp parallel
  {
    p = (get_thread_num()+1)*100;
    int l = 7;

    printf("OMP %d/%d = %d\n", get_thread_num()
                             , get_num_threads(),p);

/*
    std::cout << get_thread_num()  << "/"
              << get_num_threads() << " = " << p << "\n";
*/
  }

  printf("OUT %d\n",p);
}

#include <mpi.h>
#include <iostream>
#include <vector>
#include <chrono>

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;
  int nb_per_proc = 4;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  std::vector<float> data, buffer(nb_per_proc);
  std::vector<int>   sign(nb_per_proc);
  int count;

  if(rank==0)
  {
    data.resize(nb_per_proc*sz);

    for(std::size_t i = 0;i<data.size();++i)
    {
      data[i] = (std::rand()/float(RAND_MAX))*200.f - 100.f;
    }
  }

  if(rank == 0)
  {
    for(auto e : data)
      std::cout << e << " ";
    std::cout << "\n";
  }

  MPI_Scatter ( data.data(), nb_per_proc, MPI_FLOAT
              , buffer.data(), nb_per_proc, MPI_FLOAT
              , 0, MPI_COMM_WORLD
           );

  for(int i = 0;i<sign.size();++i)
    sign[i] = buffer[i] < 0 ? 0 : 1;

  int local_sum = 0;
  for(auto e : sign)
    local_sum += e;

  MPI_Reduce(&local_sum, &count, 1, MPI_INT
            , MPI_SUM, 0,MPI_COMM_WORLD);

  if(rank == 0)
    std::cout << "Count = " << count << "\n";

  MPI_Finalize();
}

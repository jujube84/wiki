#include <mpi.h>
#include <iostream>

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);
  int sz,rank;
  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  std::cout << "Hello world : " << rank << "/" << sz << "\n";

  int number = -1;
  if(rank == 0)
  {
    number = 999;
    std::cout << "[" << rank << "]" << number << "\n";
    MPI_Send(&number,1,MPI_INT,1, 0,MPI_COMM_WORLD);
    std::cout<< "[" << rank << "] Done" << "\n";
  }
  else if(rank == 1)
  {
    std::cout << "[" << rank << "]" << number << "\n";
    MPI_Recv(&number,1,MPI_INT,0, MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    std::cout << "[" << rank << "]" << number << "\n";
    std::cout<< "[" << rank << "] Done" << "\n";
  }

  MPI_Finalize();
}

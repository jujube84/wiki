#include <omp.h>
#include <iostream>

int main()
{
  #pragma omp parallel for schedule(runtime)
  for(int i = 0;i<21;++i)
  {
    printf("OMP %d/%d on iteration %d\n", omp_get_thread_num()
                                        , omp_get_num_threads()
                                        , i);
  }
}

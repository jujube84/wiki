#include <mpi.h>
#include <iostream>
#include <vector>

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  std::vector<float> x(2*sz),y(2),z(2);

  if(rank==0)
  {
    float k = 1;
    for(auto& v : x)
    {
      v =  k;
      k++;
    }
  }

  if(rank == 0)
  {
    std::cout << "0 - ";
    for(auto& v : x)
      std::cout << v << " ";
    std::cout << "\n";
  }

  MPI_Scatter ( x.data(), y.size(), MPI_FLOAT
              , y.data(), y.size(), MPI_FLOAT
              , 0
              , MPI_COMM_WORLD
              );

  for(auto& v : y)
      v *=10;

  MPI_Gather( y.data(), y.size(), MPI_FLOAT
            , x.data(), y.size(), MPI_FLOAT
            , sz-1
            , MPI_COMM_WORLD
            );

  if(rank == sz-1)
  {
    for(auto& v : x)
      std::cout << v << " ";
    std::cout << "\n";

    for(auto& v : y)
      std::cout << v << " ";
    std::cout << "\n";
  }

  float f = rank, s = 0.f;
  MPI_Reduce( y.data(), z.data(), z.size()
            , MPI_FLOAT, MPI_SUM, sz-1, MPI_COMM_WORLD
            );

  if(rank == sz-1)
  {
    for(auto& v : z)
      std::cout << v << " ";
    std::cout << "\n";
  }

  MPI_Finalize();
}

#include <omp.h>
#include <vector>
#include <string>
#include <cmath>
#include <iostream>
#include <chrono>

int main(int argc, char** argv)
{
  std::vector<std::string> args(argv+1,argv+argc);

  std::size_t n  = std::stoi(args[0]);
  std::size_t rr = std::stoi(args[1]);

  std::vector<double>   r(n)
                      , b(n, 1.)
                      , c(n, 1.);

  auto start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<rr;++rep)
  {
    #pragma omp parallel for
    for(std::size_t i=0;i<n;++i)
    {
      r[i] = /*std::atan(std::sqrt(std::cos(1./*/(b[i]+c[i])/*)))*/;
    }
  }
  auto stop = std::chrono::steady_clock::now();

  auto timing = stop - start;
  //std::cout << (2.2*std::chrono::duration<double,std::nano>(timing).count()/rr)/n << "\n";

  double res = 0.;
  start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<rr;++rep)
  {
    #pragma omp parallel
    {
      double local_res = 0.;

      #pragma omp for
      for(std::size_t i=0;i<n;++i)
      {
        local_res += r[i];
      }

      #pragma omp critical
      {
        res += local_res;
      }
    }
  }
  stop = std::chrono::steady_clock::now();

  timing = stop - start;
  std::cout << (std::chrono::duration<double,std::nano>(timing).count()/rr) << "\n";

  res = 0.;
  start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<rr;++rep)
  {
    #pragma omp parallel for reduction(-:res)
    for(std::size_t i=0;i<n;++i)
    {
      //r[i] = 10*r[i];
      res -= r[i];
    }
  }
  stop = std::chrono::steady_clock::now();

  timing = stop - start;
  std::cout << (std::chrono::duration<double,std::nano>(timing).count()/rr) << "\n";

  std::cout << res << "\n";
}

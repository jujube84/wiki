#include <omp.h>
#include <vector>
#include <iostream>

int main()
{
  std::vector<int> d(19);

  #pragma omp parallel for schedule(static)
  for(std::size_t i=0;i<d.size();i++)
   d[i] = omp_get_thread_num();

  for(auto e: d) std::cout << e << " ";
  std::cout << "\n";
}

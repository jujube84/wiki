#include <mpi.h>
#include <iostream>
#include <vector>
#include <chrono>

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;
  int nb_per_proc = 4;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  std::vector<int> in1, in2, out;

  if(rank==0)
  {
    in1.resize(nb_per_proc*sz);
    in2.resize(nb_per_proc*sz);
    out.resize(nb_per_proc*sz);

    for(std::size_t i = 0;i<in1.size();++i)
    {
      in1[i] = i*sz;
      in2[i] = 1+i;
      out[i] = -1;
    }
  }
  else
  {
    in1.resize(nb_per_proc);
    in2.resize(nb_per_proc);
    out.resize(nb_per_proc);
  }

  int nb  = nb_per_proc*sz;

/*
  auto start = std::chrono::steady_clock::now();
  for(int r = 0; r<100;r++)
  {*/

    MPI_Scatter ( in1.data(),nb_per_proc,MPI_INT
                , in1.data(),nb_per_proc,MPI_INT
                , 0,MPI_COMM_WORLD
                );

    MPI_Scatter ( in2.data(),nb_per_proc,MPI_INT
                , in2.data(),nb_per_proc,MPI_INT
                , 0,MPI_COMM_WORLD
                );

    for(int i=0;i<sz;++i)
    {
      if(rank == i)
      {
        std::cout << "[" << i << "] = \n";
        for(auto& e: in1)
        {
          std::cout << e << " ";
        }
        std::cout << std::endl;
        for(auto& e: in2)
        {
          std::cout << e << " ";
        }
        std::cout << std::endl;
      }
    }

    for(std::size_t i=0;i<nb_per_proc;++i)
     out[i] = in1[i] * in2[i];

    for(int i=0;i<sz;++i)
    {
      if(rank == i)
      {
        std::cout << "[" << i << "] = \n";
        for(auto& e: out)
        {
          std::cout << e << " ";
        }
        std::cout << std::endl;
      }
    }

    MPI_Gather( out.data(), nb_per_proc, MPI_INT
              , out.data(), nb_per_proc, MPI_INT
              , 0, MPI_COMM_WORLD);
/*
  }

  auto stop = std::chrono::steady_clock::now();

  auto timing = stop - start;
  std::cout << rank << " - Elasped: "
            << std::chrono::duration<double,std::micro>(timing).count()/(100) << "\n";
*/

  if(rank==0)
  {
    for(auto& e: out)
    {
      std::cout << e << " ";
    }
    std::cout << std::endl;
  }

  MPI_Finalize();
}

#include <mpi.h>
#include <iostream>
#include <vector>
#include <cstring>

struct config
{
  config() {}
  config(float u, float v, float w, char d) : x(u),y(v),z(w),c(d) {}
  virtual ~config() {}
  virtual std::vector<std::uint8_t> serialize()
  {
    std::vector<std::uint8_t> bytes(13);
    std::memcpy(&bytes[0] ,&x,sizeof(x));
    std::memcpy(&bytes[4] ,&y,sizeof(y));
    std::memcpy(&bytes[8] ,&z,sizeof(z));
    std::memcpy(&bytes[12],&c,sizeof(c));
    return bytes;
  }

  virtual void deserialize(std::vector<std::uint8_t> const& bytes)
  {
    std::memcpy(&x, &bytes[0] ,sizeof(x));
    std::memcpy(&y, &bytes[4] ,sizeof(y));
    std::memcpy(&z, &bytes[8] ,sizeof(z));
    std::memcpy(&c, &bytes[12],sizeof(c));
  }

  float x,y,z;
  char  c;
};

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  std::vector<config> cf(2);

  if(rank==0)
  {
    cf[0] = config{0.2,-5.6,3.4,'z'};
    cf[1] = config{1.,2.,3.,'a'};
  }

  std::cout << "[" << rank << "]" 
            << cf[0].x << " " << cf[0].y << " " << cf[0].z << " -> " << cf[0].c << " "
            << cf[1].x << " " << cf[1].y << " " << cf[1].z << " -> " << cf[1].c << "\n";

  auto b0 = cf[0].serialize();
  MPI_Bcast(b0.data(),b0.size(),MPI_BYTE,0,MPI_COMM_WORLD);
  auto b1 = cf[1].serialize();
  MPI_Bcast(b1.data(),b1.size(),MPI_BYTE,0,MPI_COMM_WORLD);
  cf[0].deserialize(b0);
  cf[1].deserialize(b1);

  MPI_Barrier(MPI_COMM_WORLD);

  std::cout << "[" << rank << "]" 
            << cf[0].x << " " << cf[0].y << " " << cf[0].z << " -> " << cf[0].c << " "
            << cf[1].x << " " << cf[1].y << " " << cf[1].z << " -> " << cf[1].c << "\n";

  MPI_Finalize();
}

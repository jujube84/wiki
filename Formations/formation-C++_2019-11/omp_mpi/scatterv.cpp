#include <mpi.h>
#include <iostream>
#include <vector>
#include <chrono>

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;
  int nb_per_proc = 4;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  std::vector<int> in1, in2, out;

  //if(rank==0)
  {
    in1.resize(nb_per_proc*sz+1);
    in2.resize(nb_per_proc*sz+1);
    out.resize(nb_per_proc*sz+1);

    for(std::size_t i = 0;i<in1.size();++i)
    {
      if(rank==0) in1[i] = i*sz;
      if(rank==0) in2[i] = 1+i;
      out[i] = -1;
    }
  }
  // else
  // {
  //   in1.resize(nb_per_proc);
  //   in2.resize(nb_per_proc);
  //   out.resize(nb_per_proc);
  // }

  int nb  = nb_per_proc*sz+1;
  int nz  = nb/sz;
  int rem = nb%sz;
/*
  auto start = std::chrono::steady_clock::now();
  for(int r = 0; r<100;r++)
  {*/

  std::vector<int> disp(sz);
  std::vector<int> sent(sz);
  int offset = 0;
  for(int i = 0;i<sent.size();++i)
  {
    disp[i] = offset;
    sent[i] = nz + (i<rem ? 1 : 0);
    offset += sent[i];
  }

  MPI_Scatterv( in1.data(), sent.data(), disp.data(),
                MPI_INT, out.data(), sent[rank],
                MPI_INT, 0, MPI_COMM_WORLD);

    // MPI_Scatter ( in2.data(),transmitted,MPI_INT
    //             , in2.data(),nb_per_proc,MPI_INT
    //             , 0,MPI_COMM_WORLD
    //          );

    for(int i=0;i<sz;++i)
    {
      if(rank == i)
      {
        std::cout << "[" << i << "] = \n";
        for(auto& e: out)
        {
          std::cout << e << " ";
        }
        std::cout << std::endl;
      }
    }

//    for(std::size_t i=0;i<out.size();++i)
//      out[i] = in1[i] + in2[i];

/*
    MPI_Gather( out.data(), nb_per_proc, MPI_INT
              , out.data(), nb_per_proc, MPI_INT
              , 0, MPI_COMM_WORLD);
*/
/*
  }

  auto stop = std::chrono::steady_clock::now();

  auto timing = stop - start;
  std::cout << rank << " - Elasped: "
            << std::chrono::duration<double,std::micro>(timing).count()/(100) << "\n";
*/
/*
  if(rank==0)
  {
    for(auto& e: out)
    {
      std::cout << e << " ";
    }
    std::cout << std::endl;
  }
*/

  MPI_Finalize();
}

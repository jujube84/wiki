#include <mpi.h>
#include <iostream>
#include <vector>
#include <chrono>

template<typename T> struct as_ {};

MPI_Datatype mpi_datatype(as_<int   >) { return MPI_INT;    }
MPI_Datatype mpi_datatype(as_<float >) { return MPI_FLOAT;  }
MPI_Datatype mpi_datatype(as_<double>) { return MPI_DOUBLE; }
MPI_Datatype mpi_datatype(as_<char  >) { return MPI_BYTE;   }

template<typename T>
std::vector<T> parallel_sum(std::vector<T> const& d)
{
  std::vector<T> that(d.size());

  if(d.size())
  {
    MPI_Reduce( d.data(), that.data(), d.size()
              , mpi_datatype(as_<T>())
              , MPI_SUM, 0,MPI_COMM_WORLD
              );
  }

  return that;
}
int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;
  int nb_per_proc = 4000;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  std::vector<float> data, buffer(nb_per_proc);
  std::vector<int>   sign(nb_per_proc);

  if(rank==0)
  {
    data.resize(nb_per_proc*sz);

    for(std::size_t i = 0;i<data.size();++i)
    {
      data[i] = (std::rand()/float(RAND_MAX))*200.f - 100.f;
    }
  }

  MPI_Scatter ( data.data(), nb_per_proc, MPI_FLOAT
              , buffer.data(), nb_per_proc, MPI_FLOAT
              , 0, MPI_COMM_WORLD
           );

  for(int i = 0;i<sign.size();++i)
    sign[i] = buffer[i] < 0 ? 0 : 1;

  auto res = parallel_sum(sign);
  auto cv = parallel_sum(buffer);

  if(rank == 0)
  {
    int count = 0;
    float avg = 0;
    for(auto e : res) count += e;
    for(auto e : cv ) avg += e;

    std::cout << "Count = " << count << "\n";
    std::cout << "Count = " << avg/data.size() << "\n";
  }

  MPI_Finalize();
}

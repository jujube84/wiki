#include <omp.h>
#include <map>
#include <iostream>

struct foo { int id; };

int main()
{
  std::map<unsigned int, foo> m = { {5, {2}}, {9,{25}}, {17,{99}}, {0,{78}} };

  auto ptr = m.begin();

  #pragma omp parallel for schedule(static,1) firstprivate(ptr)
  for(int i=0;i<m.size();i++)
  {
    std::advance(ptr,i);
    ptr->second.id = (omp_get_thread_num()+1);
  }

  for(auto& e : m)
  {
    std::cout << e.first << " -> " << e.second.id << "\n";
  }
}

#include <omp.h>
#include <cmath>
#include <iostream>
#include <chrono>
#include <vector>
#include "matrix.hpp"

struct box
{
  box ( std::size_t pi0,std::size_t pi1
      , std::size_t pj0,std::size_t pj1
      )
      : i0(pi0),i1(pi1),j0(pj0),j1(pj1)
  {}

  std::size_t i0,i1,j0,j1;
};

void seq_filter ( matrix const& in, matrix& out
                , int radius
                )
{
  float area = (2*radius+1)*(2*radius+1);
  for(std::size_t i=radius;i<in.height()-radius;++i)
  {
    for(std::size_t j=radius;j<in.width()-radius;++j)
    {
      for(int u=-radius;u<=radius;++u)
        for(int v=-radius;v<=radius;++v)
          out(j,i) += in(j+v,i+u);
      out(j,i) /= area;
    }
  }
}

void direct1_filter( matrix const& in, matrix& out, int radius)
{
  float area = (2*radius+1)*(2*radius+1);

  #pragma omp parallel for
  for(std::size_t i=radius;i<in.height()-radius;++i)
  {
    for(std::size_t j=radius;j<in.width()-radius;++j)
    {
      for(int u=-radius;u<=radius;++u)
        for(int v=-radius;v<=radius;++v)
          out(j,i) += in(j+v,i+u);
      out(j,i) /= area;
    }
  }
}

void direct2_filter( matrix const& in, matrix& out, int radius)
{
  float area = (2*radius+1)*(2*radius+1);

  for(std::size_t i=radius;i<in.height()-radius;++i)
  {
    #pragma omp parallel for
    for(std::size_t j=radius;j<in.width()-radius;++j)
    {
      for(int u=-radius;u<=radius;++u)
        for(int v=-radius;v<=radius;++v)
          out(j,i) += in(j+v,i+u);
      out(j,i) /= area;
    }
  }
}

void direct3_filter( matrix const& in, matrix& out, int radius)
{
  float area = (2*radius+1)*(2*radius+1);

  omp_set_nested(true);
  omp_set_dynamic(true);
  #pragma omp parallel for
  for(std::size_t i=radius;i<in.height()-radius;++i)
  {
    #pragma omp parallel for
    for(std::size_t j=radius;j<in.width()-radius;++j)
    {
      for(int u=-radius;u<=radius;++u)
        for(int v=-radius;v<=radius;++v)
          out(j,i) += in(j+v,i+u);
      out(j,i) /= area;
    }
  }
}
void box_filter ( matrix const& in, matrix& out
                , box const& b, int radius
                )
{
  float area = (2*radius+1)*(2*radius+1);
  for(std::size_t i=b.i0;i<b.i1;++i)
  {
    for(std::size_t j=b.j0;j<b.j1;++j)
    {
      for(int u=-radius;u<=radius;++u)
        for(int v=-radius;v<=radius;++v)
          out(j,i) += in(j+v,i+u);
      out(j,i) /= area;
    }
  }
}

int main(int argc, char** argv)
{
  std::vector<std::string> args(argv+1,argv+argc);

  std::size_t w  = std::stoi(args[0]);
  std::size_t h  = std::stoi(args[1]);
  std::size_t r  = std::stoi(args[2]);
  std::size_t rd = std::stoi(args[3]);
  std::size_t di = std::stoi(args[4]);
  std::size_t dj = std::stoi(args[5]);

  matrix in(w,h), out(w,h);
  for(std::size_t i=0;i<in.height();++i)
    for(std::size_t j=0;j<in.width();++j)
      in(j,i) = 1;

  auto start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<r;++rep)
  {
    seq_filter(in,out,rd);
  }
  auto stop = std::chrono::steady_clock::now();

  auto timing = stop - start;
  std::cout << "Seq filter: "
            << 2.2*std::chrono::duration<double,std::nano>(timing).count()/(r*in.width()*in.height()) << "\n";

  start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<r;++rep)
  {
    direct1_filter(in,out,rd);
  }
  stop = std::chrono::steady_clock::now();

  timing = stop - start;
  std::cout << "Direct OMP filter #1: "
            << 2.2*std::chrono::duration<double,std::nano>(timing).count()/(r*in.width()*in.height()) << "\n";

  start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<r;++rep)
  {
    direct2_filter(in,out,rd);
  }
  stop = std::chrono::steady_clock::now();

  timing = stop - start;
  std::cout << "Direct OMP filter #2: "
            << 2.2*std::chrono::duration<double,std::nano>(timing).count()/(r*in.width()*in.height()) << "\n";

  start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<r;++rep)
  {
    direct3_filter(in,out,rd);
  }
  stop = std::chrono::steady_clock::now();

  timing = stop - start;
  std::cout << "Direct OMP filter #3: "
            << 2.2*std::chrono::duration<double,std::nano>(timing).count()/(r*in.width()*in.height()) << "\n";

  start = std::chrono::steady_clock::now();
  for(std::size_t rep=0;rep<r;++rep)
  {
    std::vector<box> boxes;

    auto h1 = in.height() - rd;
    auto w1 = in.width()  - rd;

    for(std::size_t i=rd;i<h1;i+=di)
      for(std::size_t j=rd;j<w1;j+=dj)
        boxes.emplace_back(i,std::min(i+di,h1)
                          ,j,std::min(j+dj,w1)
                          );

    #pragma omp parallel for
    for(std::size_t n=0;n<boxes.size();++n)
      box_filter(in,out,boxes[n],rd);
    }
  stop = std::chrono::steady_clock::now();

  timing = stop - start;
  std::cout << "Box filter: "
            << 2.2*std::chrono::duration<double,std::nano>(timing).count()/(r*in.width()*in.height()) << "\n";
}

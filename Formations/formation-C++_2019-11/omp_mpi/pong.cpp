#include <mpi.h>
#include <iostream>

enum mode { compute, stop };

void pong(int& number, int mx, int rank, int cycle)
{
  int src = (rank+cycle-1)%cycle;
  int dst = (rank+cycle+1)%cycle;
  MPI_Status s;

  //std::cout << "[" << rank << "]: Recv from = " << src << " - Send to " << dst << "\n";
  MPI_Recv(&number,1,MPI_INT,src, MPI_ANY_TAG,MPI_COMM_WORLD,&s);
  std::cout << "[" << rank << "]: Number = " << number << "\n";
  if(number<mx && s.MPI_TAG == compute)
  {
    number++;
    MPI_Send(&number,1,MPI_INT,dst, compute,MPI_COMM_WORLD);
  }
  else
  {
    MPI_Send(&number,1,MPI_INT,dst, stop,MPI_COMM_WORLD);
  }
}

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  int number  = 0;
  int max_num = 11;
  int start_rank = 0;
  int nb_cycle = sz;
  int target = (rank+1)%nb_cycle;
  int sink   = (rank+nb_cycle-1)%nb_cycle;

  if(rank == start_rank)
  {
    std::cout << "[" << rank << "]" << number << "\n";
    MPI_Send(&number,1,MPI_INT,target, compute,MPI_COMM_WORLD);
  }

  while(number < max_num) pong(number,max_num,rank,nb_cycle);

  if(rank == (max_num+start_rank)%nb_cycle)
  {
    int dummy;
    std::cout << "[" << rank << "]: Recv " << sink << "\n";
    MPI_Recv(&dummy,1,MPI_INT,sink, stop,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
  }


  MPI_Finalize();
}

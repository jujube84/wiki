#include <mpi.h>
#include <iostream>

void pong(int src, int dst, int& number, int mx)
{
  MPI_Recv(&number,1,MPI_INT,src, MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
  if(number<mx) number++;
  MPI_Send(&number,1,MPI_INT,dst, 0,MPI_COMM_WORLD);
}

int main(int argc, char** argv)
{
  MPI_Init(&argc,&argv);

  int sz,rank;

  MPI_Comm_size(MPI_COMM_WORLD,&sz);
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  int number = 0;
  int size = sz;
  int max_num = 11;
  int next_rank = (rank+1)%size;
  int prev_rank = (rank-1+size)%size;
  int start = 0;

  if(rank == start)
  {
    std::cout << "[" << rank << "]" << number << "\n";
    MPI_Send(&number,1,MPI_INT,next_rank, 0,MPI_COMM_WORLD);
  }

  if(rank<size)
    while(number < max_num) pong(prev_rank,next_rank,number,max_num);

  if(rank==(start+max_num)%size)
  {
    MPI_Recv(&number,1,MPI_INT,prev_rank, MPI_ANY_TAG,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
    std::cout << "Number = " << number << "\n";
  }

  MPI_Finalize();
}

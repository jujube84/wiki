# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 21:29:25 2020

@author: formation Python pour utilisateur de R
"""
###### Créer une fonction qui génère un échantillon de `B=500` moyennes calculées 
# à partir d'échantillons de taille `n=10` de loi de Student à 4 ddl 

import numpy as np

n = 10
B = 100

# génération des 1000 variables
X = np.random.standard_t(4,(B,n))

X.shape   # 100 x 10

X.mean(axis=0).shape  # (10,) 
# attention, 0 est l'index de la première dimension
# on fait donc la somme sur les indices de la première dimension, ie sur les lignes
# dans R, on écrirait apply(X,2): pour chaque colonne (dimension 2), on somme

X.mean(axis=1).shape  #(100,)
# attention, 1 pour index de la deuxième dimension
# on fait donc la somme sur les indices de la deuxième dimension, ie sur les lignes
# dans R, on écrirait apply(X,1): pour chaque ligne (dimension 1), on somme

# on définit la fonction

def get_mean_t(B=100, n=10):
    X = np.random.standard_t(4, (B, n))
    return (X.mean(axis=1))  

m = get_mean_t()
m.shape

###### Enregistrer le résultat dans un fichier texte et dans un fichier binaire. Relire le résultat.

# Pour enregistrer au format texte
np.savetxt('sample.csv', m, delimiter=";" )   # enregistre dans le répertoire encours
m2 = np.loadtxt('sample.csv', delimiter=";")  # lit dans le répertoire encours
all(m == m2)  # vérifier que l'échantillon lu m2 est bien identique à m

# Pour enregistrer au format binaire
np.save('sample.npy', m)   # enregistre dans le répertoire encours
m3 = np.load('sample.npy')  # lit dans le répertoire encours
all(m == m3)  # vérifier que l'échantillon lu m3 est bien identique à m


###### Visualiser un histogramme des données
import matplotlib.pyplot as plt
plt.hist(m, density=True)

###### Définir une fonction qui calcule le nombre d'essais avant que la que 
# la moyenne de l'échantillon généré à la question 1 soit comprise entre -0.0001 et 0.0001  
eps = 1e-4
c = 1
while  abs(get_mean_t().mean()) >eps:
    c +=1
    
print(f"c= {c}")

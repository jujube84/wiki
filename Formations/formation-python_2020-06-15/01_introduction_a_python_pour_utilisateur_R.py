#!/usr/bin/env python
# coding: utf-8

# # Formation SFdS: Introduction à `Python` pour les utilisateurs de `R`.
# 
# Ou comment utiliser la 2e consonne de JuPyteR :)

# ## Python: qu'est ce que c'est?
# 
# Python est un langage qui a été créé en 1991 par Guido van Rossum.  
# C'est un langage libre, développé par une large communauté d'utilisateurs.  
# Pour résumer, le succès de Python tient particulièrement en quatre points:
# * **Simplicité et flexibilité:** le langage est simple à comprendre (à lire) et la librairie standard propose de nombreuses fonctionalités par défault. 
# * **Des librairies spécialisées:** Il existe une multitude de modules spécialisés, que l'on peut voir comme des extensions du langage Python pour réaliser certaines tâches:
#     - Django: framwork de developpement web.
#     - Numpy: librairie d'algébre linéaire.
#     - Tensorflow/Pytorch: librairie dédiée aux Réseaux de Neurones.
#     - PyTest: librairie liée au test unitaire en python.
#     - ....
#   Ces librairies sont réunies dans 2 principaux gestionnaires de packets: `pip` et `conda`, tout comme `CRAN` réunit les librairies `R` disponibles.
# * **Une facilité d'extension:** Il est en général assez simple d'utiliser/d'appeler un autre langage dans un programme Python, par exemple `R` avec `rpy2`.
# * **Un langage interprété:** Tout comme `R`, c'est un langage qui permet le developpement interactif.
# 
# En cela, `Python` et `R` sont très proches et les concepts de l'un sont facilement transposable à l'autre, mais contrairement à `R`, `Python` est  entièrement conçu en langage objet.
# 
# 
# <div style="width: 80%; padding: .5em 1.5em;
#             margin: .5em 2em .5em 2em;
#             border-radius: 12pt; border-color: black; 
#             border-style: solid;">
# <b style="font-variant: small-caps; text-decoration: underline;">Langage Interprété:</b><br>
#     Python est un langage interprété, c'est à dire qu'il se compose d'un langage et d'un interpréteur.
#     La principale différence avec les langages compilés est que la suite des instructions qui va être exécutée par le programme n'est pas connue quand celui-ci est lancé. Seules les différentes rèles qui peuvent être appliquées sont connues.
#     Le langage de programmation définit cet ensemble de régles et c'est ce que l'on appelle communément le langage Python (c'est ce qu'on va apprendre ici!).<br>
#     Il existe ensuite différents interpréteurs qui prennent ces instructions pour modifier l'état interne (comprendre les variables) du programme:
#     <ul>
#         <li> <a href=https://python.org>CPython</a>: l'interpréteur standard de Python. Écrit en C, c'est l'interpreteur de base que vous utiliserez le plus souvent.</li>
#         <li> <a href=https://jython.org/>Jython</a>: un interpréteur écrit en Java qui permet une interaction simple avec du code Java.</li>
#         <li> <a href=https://ironpython.net/>IronPython</a>: un interpréteur intégré avec  le FRamework .Net  (travail dans  le navigateur).</li>
#         <li> <a href=https://micropython.org/>microPython</a>: un interpréteur dédié au calcul embarqué, avec une optimisation drastique de la mémoire.</li>
#         <li> ....</li>
#     </ul>
# </div>
# 

# ## L'environnement de développement en Python pour la programmation scientifique
# 
# Pour obtenir un environnement de programmation scientifique, `Python` doit être combiné avec certains packages (comme `Numpy, Scipy, Pandas, Jupyter`, ...). Pour préparer votre environnement, il faut donc installer à la fois `Python` et d'autres packages. Vous avez deux options, détaillées ci-dessous. 
# 
# ### Installation d'Anaconda
# 
# [Anaconda](https://www.continuum.io/downloads) est une distribution Python qui inclut de nombreux packages python pour la programmation scientifique. C'est une façon pratique pour installer et mettre en place son environnement de travail sous n'importe quel système d'exploitation. Pour l'installer:
# 
# 1. Aller sur le site d'anaconda: [Downloads](https://www.anaconda.com/products/individual#Downloads)
# 2. Télécharger la version pour `Python 3.7` adaptée à votre ordinateur (OS et archeticture).
# 3. Une fois le setup téléchargé, l'exécuter:
#     * Accepter tous les paramètres par défaut.
# 
# ### Description d'Anaconda
# 
# Anaconda contient divers outils utiles pour `Python`. Nous nous servirons de:
# 
# * `Anaconda Navigator`: Interface graphique pour lancer les différents programmes de Anaconda et installer des librairies supplémentaires.
# * `Anaconda Prompt`: Console pour faire de même en ligne de commande.
# * `Jupyter Notebook`: Interface graphique dans le navitageur web pour écrire et exécuter du code `Python`.
# * `Spyder`: Un éditeur intégré, similaire à `Rstudio` ou `Matlab`.
# 
# Lancez Anaconda Navigator (en cherchant dans la barre de recherche de programmes si nécéssaire) et vérifiez que vous arrivez à ouvrir un notebook jupyter, comme indiqué [ici](https://docs.anaconda.com/anaconda/user-guide/getting-started/#run-python-in-a-jupyter-notebook). Vous pouvez aussi taper dans une fenêtre de commande `anaconda-navigator`.
# 
# ### Installation des librairies de calcul scientifique
# 
# Pour utiliser `Python` pour le calcul scientifique, il est nécessaire d'installer plusieurs librairies très utiles:
# 
# * `numpy`: gestion de tableaux et opérations d'algèbre linéaire de base.
# * `scipy`: librairie de calcul scientifique basée sur `numpy`.
# * `pandas`: gestion de données tabulaires de type `DataFrame`.
# * `statsmodels`: modèles statistiques définis de manière similaire à celle de `R`.
# * `rpy2`: logiciel d'interfaçage avec `R`.
# 
# Pour vérifier si un package est installé, il suffit d'essayer de l'importer en `Python`.
# Tous les packages suivants sont bien installés si l'éxecution (CTL+ENTREE) de la cellule suivante ne provoque aucune erreur.

# In[ ]:


import numpy
import scipy
import pandas
# import rpy2
import statsmodels


# Il est possible d'installer un package avec `conda` en lançant la commande `conda install -y pkg_name`. Cette commande peut être lancée dans le `Anaconda Prompt` ou dans une cellule avec `!` pour signifier que la ligne est une ligne de commande. Pour exécuter réellement la commande suivante, supprimer le commentaire # la précédant.

# In[ ]:


# !conda install -y rpy2


# Les [packages](http://conda.pydata.org/docs/using/pkgs.html) et les [environnements](http://conda.pydata.org/docs/using/envs.html) peuvent être gérés avec l'interface graphique de `Anaconda Navigator` ou avec la commande `conda` (`conda --help` et `conda env --help`).
# 
# ### Écrire d'un code en python
# 
# 
# Avec Anaconda est fourni [Spyder](https://github.com/spyder-ide/spyder), qui est un environnement de développement interactif puissant, incluant notamment un éditeur et une console. Son design est très proche de celui de `Rstudio` ou `Matlab`.  
# La plupart des **éditeurs de texte** pour la programmation fournissent au moins un support rudimentaire pour Python. Deux options populaires sont [VSCode](https://code.visualstudio.com/) et [vim](https://realpython.com/blog/python/vim-and-python-a-match-made-in-heaven/).
# 
# 
# ### Comment exécuter un code Python?
# 
# 1. Python  
#    `$ python script.py`
# 
# 2. IPython: shell interactif   
#    `$ ipython`  
#    Très pratique pour tester des algorithmes, explorer des données,... [Documentation ici](http://ipython.readthedocs.io/en/stable/).  
# 
# 4. dans l'environnement Spyder  
# 
# 4. Jupyter notebook / Jupyter Lab:  
#    Application web qui permet de créer des documents contenant du code, du texte, et des résultats (traces, graphiques).
#    Utilise le kernel IPython. [Documentation ici](http://jupyter-notebook.readthedocs.io/en/latest/)   
#    Note: [lien utile](http://help.pythonanywhere.com/pages/IPythonNotebookVirtualenvs) pour utiliser Jupyter notebook dans un environnement virtuel.
# 
# 
# Ce fichier est un exemple de notebook jupyter. Son extension est **.ipynb**. Il contient plusieurs types de cellules, dont <ul><li>les cellules Markdown de texte (comme celle-ci). Double cliquer pour activer.
#     <li>les cellules de code, précédée par `I[]`.  Cliquer pour activer.
#         </ul>
#         Elles s'exécutent quand elles sont activées en tapant `CTL+ENTREE`. Utiliser l'interface web pour gérer les cellules.

# ## Les bases du langage `Python`
# 
# Commençons par préciser comment accéder à la documentation
# * [manuel de référence](https://docs.python.org/fr/3.7/reference/): structure et sémantique interne
# 
# * [bibliothèque standard](https://docs.python.org/fr/3.7/library/): sémantique des objets natifs secondaires, des fonctions, et des modules
# 
# * commande en ligne : `help('commande')` ou `commande?`
# 
# Affichage d'information en cours de script: `print`
# 
# * Affiche dans stdout.
# * Peut prendre plusieurs arguments.
# 
# Dans jupyter, seul le résultat de la dernière ligne est affiché (sauf si c'est `None`).

# In[ ]:


print(1)
2
print(3, 'hello')
4
a = 10
b = 3
a, b
c = a-b
a, b, c


# ### Types et structures de base
# Sont très similaires à ceux de R. Ci-dessous quelques différences

# In[ ]:


# python
2**4      
21 // 5   
21 % 5    

y = 1.64   
y += 1
b = False/True # and or not # noter que & | ! servent pour les tests bit à bit
c = 'a'        
s = 'Hello world'    
type(s)        # str

m = '''chaîne de caractères
 sur plusieurs lignes''' 
print(m)
m


# Noter quelques autres différences avec R ci dessous:

# In[ ]:


x = 1
a = 'c'
x, y, a


# In[ ]:


z, t = 2*x, y+2
z, t


# In[ ]:


z, t = t, z
z, t


# Python permet de gérer différents types de containers
# * la liste `list`: collection d'éléments hétérogènes
# * le n-uplet `tuple`: liste non modifiable
# * le dictionnaires `dic`: collections d'éléments accessibles par une clé d'identification
# * l'itérateur `range`: définit la spécification de suites arithmétiques, en l'utilisant à la volée
# 
# Pour les containers, chacun peut être instancié de 2 façons:
# * soit avec l'interface fonctionnelle `str, tuple, list, dict, set,...`
# * soit avec une notation spécifique `'ab', (a1, a2), [a1, a2], {k1: a1, k2: a2}, {a1, a2}`
# 
# Attention, l'index du premier élément est **0** (contrairement à R où c'est 1)

# #### Exemple de `tuple` 

# In[ ]:


a = (1, 2, False)    # tuple 
a


# #### Exemple de `list`

# In[ ]:


l = [1, 2, 2.3, [], "c"]
l[2]                            # 2.3
l[3] = [3.14,"b"]
l[2:4]                          # [2.3, [3.14,"b"]]


# In[ ]:


l[5]                            # out of range


# In[ ]:


1:3                             # SyntaxError


# In[ ]:


del l[0]                        # supp. 1er item
l[-1]                           # dernier item
l.append(78.22)
l


# #### Exemple d'utilisation de `range`

# In[ ]:


# 1:5      # SyntaxError
print(range(5)) 


# In[ ]:


sum(range(5))


# In[ ]:


list(range(1,5))


# #### Exemple de dictionnaire
# A rapprocher  à `d = list(a=1, b="hello", c=list(NULL))` dans R

# In[ ]:


d = {'a': 1,         # dict
'b': "hello",    
'c': {}          
    }
d = dict(a=1, b="hello", c={})
d['b']
d.keys()


# <div style="width: 80%; padding: .5em 1.5em;
#             margin: .5em 2em .5em 2em;
#             border-radius: 12pt; border-color: black; 
#             border-style: solid;">
#     <b style="font-variant: small-caps; text-decoration: underline;">Application:</b>
#      <ol>
#     <li> Créer une liste <code>semaine</code> avec les jours de la semaine
#     <li> Faire afficher sur la même ligne <code>mardi</code>  et <code>dimanche</code>
#     <li> Quel est le type de l'objet correspondant?
#     <li> Avec des commandes de sélection les plus simples possibles, faire afficher les jours de travail, puis les jours du week-end
#     <li> Renverser la liste semaine avec le fancy indexing?
#     <li> Prendre 1 lettre sur 3 de <code>lundi</code> à partir de la lettre <code>u</code>
#     <li> En utilisant la documentation de la commande <code>in</code>, tester la présence de <code>vendredi</code> dans la liste
#     <li>On peut vérifier la réponse avec <code>type</code>
#         </ol
# </div>

# In[ ]:


# %load solutions/solution_q01.py


# Les containers ont aussi beaucoup de méthodes utiles pour les utiliser:
# 
# * List: `append(elem)`, `pop(idx)`, `clear()`, ...
# * Dict: `update(Dict)`, `pop(key)`, `clear()`, ...
# * Set: `union(Set)`, `diff(Set)`, `clear()`, ...
# 
# Toutes ces méthodes sont listées ici: https://docs.python.org/3/tutorial/datastructures.html

# ### Structure de contrôle et syntaxe
# 
# Une particularité importante de python est que c'est un langage indenté.  
# Les structures de contrôle `if/for/while` s'écrivent donc sous forme de blocs  
# et les espaces jouent un rôle important:

# In[ ]:


if True:
    print("if")
elif True:
    print("elif")
else:
    print("failed?")
    
for i in range(3):
    print("for", i)

c = 2
while c > 0:
    c -= 1  # équivalent de c = c - 1
    print("while", c)


# In[ ]:


# Le nombre d'espace importe peu mais doit être
# consistant au sein d'un bloc.
# Une convention est d'utiliser 4 espaces.
semaine = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi']
if 'lundi' in semaine:
            print('test')
else:
  print('toto')


# In[ ]:


# Si ce n'est pas consistant, on obtient une erreur.
1 + 1
  2 + 2


# <div style="width: 80%; padding: .5em 1.5em;
#             margin: .5em 2em .5em 2em;
#             border-radius: 12pt; border-color: black; 
#             border-style: solid;">
#     <b style="font-variant: small-caps; text-decoration: underline;">Exercice:</b>
#         Calculer une approximation de π par la formule de Wallis:
#         <img src=https://wikimedia.org/api/rest_v1/media/math/render/svg/df59bf8aa67b6dff8be6cffb4f59777cea828454>
# </div>

# In[ ]:


# %load solutions/solution_exo00.py


# ### Utilisation des fonctions en `Python`
# 
# * Les fonctions sont également des blocs indentés.
# * Les arguments peuvent être passés dans l'ordre ou avec leurs noms.
# * On peut retourner plusieurs arguments et profiter de l'expansion des tuples

# In[ ]:


def my_func(x, y, c=5, d=9):
    print("Call func with:", x, y, c, d)
    return x + y, c - d

t = my_func(1, 2, c=12)
print("t:", t)
a, b = my_func(y=1, x=2, d=2)
print(f"a={a}, b={b}, b^2={b*b}")


# Noter le `f` dans la syntaxe du `print`: la string contient du texte et le contenu de variables  indiquées par des `{}`

# ### Utilisation des librairies en `Python`
# 
# Comme en `R`, les librairies doivent être importées pour être utilisées:
# 
# * `import lib` importe toute la librairie `lib` et ses fonctions sont disponibles sous la forme `lib.func1, lib.func2, ...`
# * `from lib import subpart` importe une partie de la librairie `lib`, comme une sous librairie ou une fonction/classe spécifique.
# * Il est possible de donner un alias à la libraire avec le mot clé `as`

# In[ ]:


import numpy as np
from sklearn.linear_model import Ridge

model = Ridge()
model.fit(X, y).score(X, y)


# In[ ]:


help("sklearn.linear_model.Ridge.score")


# In[ ]:


import sklearn.linear_model as lm

model = lm.Ridge()
model.fit(X, y).score(X, y)     # R^2


# In[ ]:


help(lm.Ridge.score)


# In[ ]:


import numpy as np 

np.cos(np.pi / 3.0) 

M = np.array([[1,3],[8,2],[-1,2.5]])
M.shape                  # (3, 2)
M.ndim, len(M), size(M)  # (2, 3, 6)

t = np.arange(10)        # cf. seq de R  
type(t)                  # numpy.ndarray
np.exp(t/4) + np.pi      # opérations vectorisées

X = np.random.randn(100, 10)

X*X                      # comme R, terme à terme
y = X.dot(t)             # X%*%t

from scipy import linalg
linalg.inv(X[0:10,])     # inverse (cf solve dans R)


# ### Quelques commandes basiques

# In[ ]:


import os 
mydir = os.getcwd()        # cf getwd
mydir


# In[ ]:


os.chdir(mydir)            # cf setwd
locals() #mais pas exactement la même chose que ls() dans R...
del(a)   #rm dans R


# #### load/save (binaire)

# In[ ]:


import pickle
with open('myobj.pkl', 'wb') as f:
   pickle.dump(semaine, f)

with open('myobj.pkl', 'rb') as f:
   obj = pickle.load(f)  

# np.ndarray
np.save('x.npy', x)
array = np.load('x.npy')

# pandas.DataFrame... à voir tout de suite après !
# df.to_pickle('mydf.pkl')
# df = pd.read_picke('mydf.pkl')


# #### read/write (texte)
# avec cette syntaxe, le fichier est automatiquement fermé en sortie du bloc

# In[ ]:


# écriture
nom = "essai-ecriture.txt"
with open(nom, 'w') as fic:
    for i in range(1, 3):
        fic.write('ligne {}\n'.format(i))


# In[ ]:


#lecture basique
with open(nom, 'r') as fic:
    liste = fic.read()   #str
    print(liste)


# In[ ]:


with open(nom, 'r') as fic:
    liste = fic.readlines()# list
    print(liste)


# #### Variables aléatoires et fonctions de probabilité
# * module `random`  pour des générations unitaires
# * module `scipy.stats`, [doc](https://docs.scipy.org/doc/scipy/reference/stats.html)
# * module `numpy.random` pour générer des tableaux complets d'échantillons

# In[ ]:


# fonctions de probabilité
from scipy import stats
stats.norm.cdf(1.65, 
        loc = 0, scale = 1)
stats.norm.sf(2.1)  # P(N>2.1)
stats.norm.ppf(0.95) # quantile


# In[ ]:


# génération
stats.norm.rvs(loc=0, scale=1, size=3, random_state = 2020)


# In[ ]:


#numpy, le plus utilisé !
import numpy as np
np.random.seed(42)
X = np.random.normal(loc=2, scale=1.5, size=100)


# ### Visualisation

# In[ ]:


# plot an histogram
rvs = np.random.standard_t(4, size=1000)
plt.hist(rvs, color='green', alpha=.3, bins=50, density=True)

# plot density distribution
x = np.linspace(-5, 5, 1001)
pdf_normal = stats.distributions.norm.pdf(x)
plt.plot(x, pdf_normal, label='Normal')
pdf_student = stats.distributions.t.pdf(x, df=4)
plt.plot(x, pdf_student, c='C2', linestyle='--', label='t-Student')

# Add a legend
plt.xlim(-6, 6)
plt.ylabel('Density')
plt.legend()
plt.savefig('histogram.pdf')


# Partager la zone de tracé

# In[ ]:


import matplotlib.pyplot as plt 
fig, axes = plt.subplots(ncols=2,
                         nrows=2)

ax = axes[0, 0]  # select first subplot
ax.scatter([-2, 2], [-1, 1],
           marker='o')
ax.hlines([-2, 2], -10, 10,
          linestyle='--')


# <div style="width: 80%; padding: .5em 1.5em;
#             margin: .5em 2em .5em 2em;
#             border-radius: 12pt; border-color: black; 
#             border-style: solid;">
#     <b style="font-variant: small-caps; text-decoration: underline;">Application</b>
#         <ol>
#     <li>Créer une fonction qui génère un échantillon de <code>B=500</code> moyennes calculées à partir d'échantillons de taille <code>n=10</code> de loi de Student à 4 ddl 
#         <li> Visualiser un histogramme des données
# <li> Enregistrer le résultat dans un fichier texte et dans un fichier binaire. Relire le résultat.
# <li> Définir une fonction qui calcule le nombre d'essais avant que la que la moyenne de l'échantillon généré à la question 1 soit comprise entre -0.1 et -0.1  
#     </ol>
# </div>
# 

# ## Analyse statistique avec `Python`
# 
# On va maintenant faire une analyse statistique sur le dataset Boston.
# 
# ### Chargement de la librairie et des données
# 
# Pour manipuler des données tabulaires, on utilise la librairie `pandas`:

# In[ ]:


from scipy import stats         # calcul scientifique et stats
import numpy as np              # array, vectorisation,indexation
import pandas as pd             # DataFrame
import matplotlib.pyplot as plt # visualisation
import statsmodels.api as sm    # ajustement modèle comme en R
import statsmodels.formula.api as smf # formules modèles R 


# ### le DataFrame de pandas
# Equivalent Python du data.frame R

# In[ ]:


# python
df = pd.DataFrame(
 {'A': [1, 2, 3], 
  'B': ['a','c','b'], 
  'C': [True, False, False], 
  'D': [8.5, 3.3, 45]},
  index = ['a1', 'a2', 'a3'])

df.to_csv("mon_csv.csv")

df = pd.read_csv( "mon_csv.csv",
    delimiter=',',
    index_col=0)

# récupérer un dataset de R !
Boston=sm.datasets.get_rdataset(
     "Boston", "MASS", cache=True)
Boston.data.head()   #DataFrame Boston


# In[ ]:


# Import the numpy and pandas library
import numpy as np
import pandas as pd

# Import matplotlib
import matplotlib.pyplot as plt


# Globalement la même chose que R

# In[ ]:


df.dtypes
df.index
df.columns
(n, p) = df.shape
n = len(df)
p = len(df.columns)
df.head()
df.describe()  # var. num. uniquement


# In[ ]:


df.describe(include='object')


# In[ ]:


df.describe(include='all')


# In[ ]:


# modification
df.columns=["num", "char", "bool", "dec"]
df.head()


# In[ ]:


df.sort_values(['char'])


# In[ ]:


# sélection 
df[['char','bool']]       #idem
df.loc[:,['char','bool']] #idem

df.iloc[1-1:2,0:2]
df.iat[2,3]

df[df.dec > 4]
df.query('dec  > 4')


# mais deux subtiles différences et un nouveau type `Series` qui ressemble beaucoup à un vecteur de R

# In[ ]:


# DataFrame
df[1:3]  # ligne 2 et 3

df[['char','bool']] # col 2 et 3
df.loc[:,['char','bool']] # idem

df[['char']]  # DataFrame col 3
df[2:3]       # DataFrame ligne 3

# Series
df['char'] 


# des visualisations spécifiques

# In[ ]:


# plot de Y variable quanti
# pd.Series.plot?

Y=Boston.data["crim"]
# Y.plot?
Y.plot(kind='box')
Y.hist

# scatter plot multidimensionel
import seaborn as sns
sns.pairplot(Boston.data[['crim','zn','indus']])


# ### Tests
# Les tests statistiques classiques sont disponibles dans le module `scipy.stats`,[doc](https://docs.scipy.org/doc/scipy/reference/stats.html)

# In[ ]:


stats.shapiro(Y)


# In[ ]:


stats.kstest(Y, 'norm')


# ### Régression

# In[ ]:


import statsmodels.api as sm
import statsmodels.formula.api as smf

results = smf.ols("medv ~ lstat", data=Boston.data).fit()
results.summary()

print(results.params)         # cf lm()$coefficients
print(results.conf_int())     # cf confint(lm())
print("\n prediction ", results.predict()[:10]) # cf predict

# prediction en valeur moyenne
newdata = pd.DataFrame({'lstat': [5, 10, 15]})
prediction_results = results.get_prediction(newdata)
prediction_results.summary_frame()


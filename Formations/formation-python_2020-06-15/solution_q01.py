# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 21:29:25 2020

@author: formation Python pour utilisateur de R
"""
###### Créer une liste `semaine` avec les jours de la semaine
semaine=["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche",]

###### Faire afficher sur la même ligne `mardi`  et `dimanche`
# attention, l'index commence à 0
semaine[1], semaine[6]
#OUT: ('mardi', 'dimanche') 

semaine[1], semaine[-1]  # on indique que dimanche est le dernier jour de la semaine

###### Quel est le type de l'objet correspondant?
# entre parenthèses, c'est une tuple

type((semaine[1], semaine[6]))


###### Avec des commandes de sélection les plus simples possibles, faire afficher les jours de travail, puis les jours du week-end
# fancy indexing: [deb:fin:step] définit les entiers de `début` inclus à `fin` exclus par pas de `step`
# si `début` est omis, c'est 0
# si `fin` est omis, c'est jusqu'à la dernière valeur
# si `step`est omis, c'est par pas de 1


semaine[:5]      # jours de la semaine
semaine[5:]      # we

# on peut utiliser des indices négatifs
semaine[:-2]    # jours de la semaine

# on peut utiliser la fonction slice
semaine[slice(0,len(semaine)-2,1)]  # jours de la semain

###### Renverser la liste semaine avec le fancy indexing
semaine[::-1] 

###### Prendre 1 lettre sur 3 de `lundi` à partir de la lettre `u`
semaine[0][1::3]

######  En utilisant la documentation de la commande `in`, tester la présence de `vendredi` dans la liste
help('in')
'vendredi' in semaine  # True
'vend' in semaine      # False
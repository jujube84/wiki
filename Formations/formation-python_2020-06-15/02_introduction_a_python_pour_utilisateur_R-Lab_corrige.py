#!/usr/bin/env python
# coding: utf-8

# # TP Formation SFdS `Python` pour les utilisateurs de `R`
# 
# Ou comment utiliser la 2e consonne de JuPyteR :)

# L'objectif du TP est d'utiliser le jeu de données `Boston` pour effectuer une régression multiple.
# Le TP aborde
# * la lecture de données
# * statistiques descriptives
# * visualisation
# * régression
# * appel de R dans Python
# 
# On commence par importer les librairies utiles

# In[ ]:


import os                       # pour changer de répertoire
from scipy import stats         # calcul scientifique et stats
import numpy as np              # array, vectorisation,indexation
import pandas as pd             # DataFrame
import matplotlib.pyplot as plt # visualisation
import statsmodels.api as sm    # ajustement modèle comme en Rb
import statsmodels.formula.api as smf # formules modèles R 


# ## Lecture du jeu de données
# 
# cf slide 34

# In[ ]:


# directement en lisant le fichier csv sur le web
df = pd.read_csv(
    "https://forge.scilab.org/index.php/p/rdataset"
    "/source/file/master/csv/MASS/Boston.csv",
    delimiter=',', index_col=0
)
df.head()


# In[ ]:


# directement à partir d'un fichier stocké en local
import os
os.chdir("C:\\Users\path") # à personnaliser
df = pd.read_csv("Boston.txt",delimiter=" ")
df.head()


# In[ ]:


# en chargeant le jeu de données via le package MASS de R
Boston = sm.datasets.get_rdataset("Boston", "MASS", cache=True)   # récupération du dataset
df = Boston.data    # récupération du contenu sous forme de dataframe
df.head()


# ## Description élémentaire du `DataFrame`

# ### Récapitulatif sur la taille des objets et l'affichage des premiers éléments
# 
# <!-- |   |  liste  | numpy array  | dataframe |
# |---|---|---|---|
# | **len(obj)**       | nombre d'éléments  | nombre de lignes  | nombre de lignes  |
# | **obj.shape**      |  ERREUR | tuple (taille)  |  (nb lignes, nb colonnes)  |
# | **obj.ndim**       |  ERREUR | dim  |  (nb lignes, nb colonnes)  | -->
# 
# Pour les *listes* (`l`) :
# + `len(l)` : longueur de la liste `l`
# 
# Pour les *numpy array* (`arr`) :
# + `len(arr)` : nombre de lignes du tableau
# + `arr.shape` : taille du tableau (tuple)
# + `arr.ndim` : dimension du tableau (*ex*: `2` pour un tableau 2D)
# 
# Pour les *dataframe* (`df`) :
# + `len(df)` : nombre de lignes du *dataframe*
# + `df.shape` : taille du dataframe (tuple)
# + `df.ndim` : dimension du dataframe
# + `df.head(n)` : affiche les `n` premières lignes (defaut : `n=5`)
# + `df.tail(n)` : affiche les `n` dernières lignes (defaut : `n=5`)

# ### lister le nom des colonnes
# 
# cf slide 35

# In[ ]:


df.columns


# ### Interpréter les commandes suivantes
# Penser à utiliser la commande `df.replace?` pour obtenir l'aide et voir slide 35 pour la méthode `describe`

# In[ ]:


df['chas_cat'] = df['chas'].replace({0: 'no river', 1: 'river'})
df.describe(include='object')


# + `df['chas_cat'] = df['chas'].replace({0: 'no river', 1: 'river'})`
# 
# Création d'une nouvelle colonne (`chas_cat`) dans le dataframe à partir de la colonne `chas` en remplaçant les `0` par `"no river"` et les `1` par `"river"`
# 
# + `df.describe(include='object')`
# 
# Description de l'ensemble des variables de type `object` (donc pas les variables numériques) : ici, seule la variable  `'chas_cat'` est concernée
# 

# ###  Table de comptage
# Utiliser une méthode de l'objet `series` pour déterminer la table de comptage de la variable `chas_cat` (cf slide 38).

# In[ ]:


df['chas_cat'].value_counts()


# ### Quel est le type de la variable `Y = df['medv]` ?
# 
# cf slide 11 

# In[ ]:


Y = df['medv']
type(Y) # Series


# ### Variance
# Calculer la variance de la variable `medv` par sa formule, puis comparer avec avec une fonction du logiciel; calculer la déviation standard.

# ##### A la main

# In[ ]:


print(Y.mean())
 # estimateur sans biais de la variance
( (Y-Y.mean())**2 ).sum() / (len(Y) - 1)


# ##### Avec la méthode `var`

# In[ ]:


Y.var()


# #### Déviation standard

# In[ ]:


print(np.sqrt(Y.var()))  # en prenant la racine carrée de la variance (fonction sqrt de numpy)
print(Y.std())           # en appelant directement la méthode


# ### Prix moyen en fonction de la position à la rivière
# cf slide 37 pour la manipulation des `DataFrame`.

# In[ ]:


# no river
Y[df.chas_cat == 'no river'].mean()


# In[ ]:


# river
Y[df.chas_cat == 'river'].mean()


# In[ ]:


# on peut aussi faire les deux d'un coup! Equivalent de `tapply`
df['medv'].groupby(df['chas_cat']).mean()


# ## Visualisation des données
# 
# * partager la fenêtre de visualisation en deux parties
# 
# * visualiser `Y`sous forme d'un boxplot, puis d'un histogramme.
# 
# * superposer la densité de la loi gaussienne estimée
# 
# * faire afficher la documentation de la fonction plot
# 
# * faire afficher un scatter plot croisant les quatre premières variables

# ### Premiers plots (boxplot et histogramme)
# cf slides 31 et 32 sur matplotlib

# In[ ]:


# Partage de la fenêtre en 2
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(10,5))

# Boxplot sur la première sous-fenêtre (axes[0])
Y.plot(kind='box', ax=axes[0]) 

# Histogramme sur la seconde sous-fenêtre (axes[1])
Y.plot(kind='hist', ax=axes[1], density=True) 

# Plot de la densité par dessus l'histogramme (seconde sous-fenêtre)
x = np.linspace(0,50,100)  # abscisses pour le calcul de la densité gaussienne
y = stats.norm.pdf(x, loc=Y.mean(), scale=Y.std()) # calcul de la densité de la gaussienne estimée
axes[1].plot(x, y)
plt.show()


# ### Documentation de la fonction `plot`
# cf slide 9

# In[ ]:


help(pd.Series.plot)
# aussi possible avec la commande `pd.Series.plot?`


# ### Scatter plot multivarié
# cf slide 37 pour la manipulation des `DataFrame` et p 39 pour la visualisation multivariée.

# In[ ]:


import seaborn as sns
subset = df.iloc[:,:4]  # Sélection des 4 premières colonnes
sns.pairplot(subset)    # Pairplot via la librairie seaborn 


# ## Régression linéaire simple
# 
# Premier exemple pour montrer le fonctionnement de `ols` du module `statsmodels.formula.api` qui permet d'utiliser les mêmes formules que dans R.
# 
# * Réaliser une régression du prix `medv` en fonction du statut `lstat`
# * afficher les résultats
# * calculer les intervalles de confiance des paramètres
# 
# cf slides 41 et 42 sur la librairie `statsmodels`

# ### Estimation

# In[ ]:


# on relit d'abord le jeu de données qui avait été modifié
Boston = sm.datasets.get_rdataset("Boston", "MASS", cache=True)   # récupération du dataset
df = Boston.data                                                  # récupération du contenu sous forme de DataFrame
df.shape

# on estime la régression simple
results = smf.ols('medv ~ lstat', data=df).fit()
results.summary()  # sorties très similmaires à celles de R


# ### Affichage d'informations

# In[ ]:


# les paramètres estimés
print(results.params)

# le R2
print("R2 = ",results.rsquared)

# SCM/dfSCM
print('SCM/dfSCM = ',results.mse_model)

# SCR/DFscr
print('SCR/dfSCR = ',results.mse_resid)

# RSE
print('residual standard error = ',np.sqrt(results.mse_resid))

# F
print('F = ',results.mse_model/results.mse_resid)

# prédiction sur 5 premières observations du jeu de données
print("\n prediction \n", results.predict(df[0:5]))

# résidus bruts des 5 premières observations
print("\n résidus \n", results.resid[0:5])

# intervalle de confiance
print(results.conf_int())


# ### Visualisation
# Interpréter le code suivant

# In[ ]:


import matplotlib.pyplot as plt

# définition de la fenêtre
fig, ax = plt.subplots(figsize=(12,8))

# définition des axes
ax.scatter(df['lstat'], df['medv'], label='Observation')

# affichage des points et de la droite de régression
fig = sm.graphics.abline_plot(
    model_results=results, ax=ax, label='ajustement',
    color='C1', linewidth=4)

# estimation non paramétrique de la régression (lowess)
import statsmodels
lowess = statsmodels.nonparametric.smoothers_lowess.lowess(
    df['medv'], df['lstat'])
plt.plot(lowess[:, 0], lowess[:, 1], 'C2', lw=4,
         label='Lowess')

# ajout de la légende et des labels
plt.legend(fontsize=18)
plt.xlabel('status', fontsize=18)
plt.ylabel('prix', fontsize=18)
plt.title("prix en fonction du statut", fontsize=18)


# ### Equivalent des plots dans R

# In[ ]:


# on coupe l'écran en 4
fig, axes=plt.subplots(ncols=2, nrows=2, figsize=(10,10))

# 1- résidus bruts versus ajustement
axes[0,0].scatter(results.predict(),results.resid)

lowess = statsmodels.nonparametric.smoothers_lowess.lowess(results.resid,results.predict())
axes[0,0].plot(lowess[:, 0], lowess[:, 1], 'C2', lw=4, linestyle='--',
         label='Lowess')
axes[0,0].set_xlabel('prediction')
axes[0,0].set_ylabel('résidu brut')

infl = results.get_influence()
dir(infl)  # pour connaître les attribus disponibles
# résidus standardisés
infl.resid_studentized
# 2- normal qqplot
p = sm.qqplot(infl.resid_studentized,line='45', ax=axes[0,1])
axes[0,1].set_title("Normal qqplot")


# 3- scale location
axes[1,0].scatter(results.predict(),np.sqrt(np.abs(infl.resid_studentized)))
lowess = statsmodels.nonparametric.smoothers_lowess.lowess(np.sqrt(np.abs(infl.resid_studentized)),results.predict())
axes[1,0].plot(lowess[:, 0], lowess[:, 1], 'C2', lw=4, linestyle='--',
         label='Lowess')
axes[1,0].set_title("scale/location")

# 4- leverage
leviers = infl.hat_matrix_diag
axes[1,1].scatter(leviers, infl.resid_studentized)
lowess = statsmodels.nonparametric.smoothers_lowess.lowess(np.sqrt(np.abs(infl.resid_studentized)), leviers)
axes[1,1].plot(lowess[:, 0], lowess[:, 1], 'C3', lw=2,
         label='Lowess')
axes[1,1].set_title("leverage")

#seuil levier
#seuil=2*(len(df) / len(y_train))
#print(df.index[leviers > seuil],leviers[leviers > seuil])


# Il existe aussi une fonction de `statsmodel` qui visualise l'influence.

# In[ ]:


p = sm.graphics.influence_plot(results)


# ## Echantillon d'apprentissage et échantillon de test
# 
# Vérifier que le code suivant permet de partager les données en un échantillon d'apprentissage et un échantillon de test.

# In[ ]:


# génération des indices
index = df.index.values.copy()   # copie de la liste des index du DataFrame sous forme d'un tableau numpy
print('type de index=', type(index))

np.random.seed(2020)             # initialisation du générateur aléatoire
np.random.shuffle(index)         # permutation aléatoire des index

# indices correspondant respectivement à l'échantillon d'entraînement et de test
train_index, test_index = sorted(index[:int(2/3*len(index))]), sorted(index[int(2/3*len(index)):])

print('longueur de train=', len(train_index), 'longueur de test=', len(test_index))
print('type de train_index = ',type(train_index))
print(train_index[0:10])
print(test_index[0:10])


# ## Régression multiple
# * Estimer la régression du prix `medv` en fonction  de toutes les variables sur l'échantillon d'apprentissage, afficher les résultats, calculer les intervalles de confiance des paramètres
# * calculer la performance sur l'échantillon de test

# ### Estimation et résultats

# In[ ]:


# codage de la formule avec toutes les variables (cf `paste`de R)
cols_list = df.drop(columns=['medv']).columns   # liste de tous les noms de variables
print(cols_list)
print(" + ".join(cols_list))                  # concaténation (équivalent de paste)
formula = "medv ~ " + " + ".join(cols_list)   
formula


# In[ ]:


res_all = smf.ols(formula, data=df, subset=train_index).fit()

# intervalle de confiance
print(res_all.conf_int())

# summary
res_all.summary()
# noter l'avertissement, intéressant, qu'on ne trouve pas dans R
# [2] The condition number is large, 1.51e+04. This might indicate that there are
# strong multicollinearity or other numerical problems.


# ### Performances sur l'échantillon de test

# In[ ]:


predictions = res_all.predict(df.loc[test_index])   
print('type predictions = ', type(predictions))    # Series

# à l'affichage, la première colonne est le label de l'observation, la deuxième la prédiction
predictions.head()


# In[ ]:


print( "calcul à la main de l'erreur de pred= ", ((df['medv'].loc[test_index] - predictions)**2).mean() )

# scikit_learn comporte de nombreuses fonctionnalités
from sklearn.metrics import mean_squared_error
mean_squared_error(df['medv'].loc[test_index], predictions)  # performances (mean square error)


# ### Remarque
# 
# `statsmodels`  propose aussi une API fonctionnelle pour les modèles comme le Ordinary Least Square: [`OLS` model](https://www.statsmodels.org/stable/generated/statsmodels.regression.linear_model.OLS.html?highlight=ols#statsmodels.regression.linear_model.OLS).
# 
# **Attention :** Pour l'utilisation d'OLS, l'intercept n'est pas inclus par défaut !

# ##  Selection de Variables: utilisation d'un module R
# 

# Il n'y a pas de package python équivalent pour la librairie `leaps`. Si celle-ci est installée sur R, il est possible de l'appeler directement en python avec la librairie `rpy2` qui permet de faire une interface simple de `Python` à `R`. Il faut commencer par installer ce package dans `conda`. Cette commande peut prendre un certain temps.
# 
# * On peut lancer l'installation dans une fenêtre `anaconda prompt` qui peut être appelée via la barre de recherche de programme sous windows ou en tapant `anaconda-prompt` dans une fenêtre de commandes sous linux.
# 
# * On propose ici une version en ligne, en utilisant le mécanisme de `try/except` pour ne faire l'installation qu'une seule fois. Si `import` ne  renvoie pas d'erreur, `rpy2` est déjà installé. En  cas d'erreur, la ligne de commande d'installation est  executée  (`!` au début de la ligne  signifie à Jupyter que cette ligne est une commande bash). 

# In[ ]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# In[ ]:


# 
try:
    import rpy2, tzlocal
except Exception:
    # tzlocal est necessaire pour utiliser pandas2ri
    get_ipython().system('conda install -y rpy2 tzlocal')


# Ensuite:
# 
# - on charge le module `robjects` de la librairie `rpy2` qui va servir à tranformer les fonctions `R` en fonctions `Python`.
# - on active le convertisseur `pandas2ri`, qui permet de convertir les `pandas..DataFrame` automatiquement en `DataFrame` `R` quand on les passe à une fonction `R`.
# - on installe et on importe `leaps` dans l'installation `R` de `conda` en utilisant `rpackages`.

# In[ ]:


from rpy2 import robjects

# cela permet de convertir automatiquement les objets pandas and object R
from rpy2.robjects import pandas2ri
pandas2ri.activate()

# On s'assure d'abord que la librairie `leaps` est
# bien installé dans l'interface R de conda
import rpy2.robjects.packages as rpackages

utils = rpackages.importr("utils")

# On utilise le mecanisme de try/except pour ne faire
# l'installation qu'une seule fois. Si `importr` ne
# renvoie pas d'erreur, `leaps` est deja installé. En
# cas d'erreur, on installe `leaps` et on l'import
# à la fin.
try:
    rpackages.importr('leaps')
except:
    utils.chooseCRANmirror(ind=1)
    utils.install_packages("leaps", dependencies=True)
    rpackages.importr('leaps')


# On peut maintenant charger les fonctions `regsubsets` et `summary` depuis `R` et les appeller en `Python`.
# 
# **Note:** il arrive qu'il y ait une mauvaise interaction entre les librairies chargées précédemment et `rpy2`. Dans ce cas, ne pas hésiter à relancer le kernel et à faire tourner les cellules à partir du début de cette section.

# In[ ]:


regsubsets = robjects.r['regsubsets']
rsummary = robjects.r['summary']


# In[ ]:


Y = df['medv']
X = df.drop(columns=['medv'])
recherche = regsubsets(X, Y, nvmax=9)


# In[ ]:


reg_summary = rsummary(recherche)
reg_summary = dict(zip(reg_summary.names, list(reg_summary)))


# In[ ]:


# Cette cellule définie un contexte qui permet
# d'afficher le résultat du plot de R directement
# dans le notebook plutot que dans une fenêtre à part.

from contextlib import contextmanager
from rpy2.robjects.lib import grdevices
from IPython.display import Image, display

@contextmanager
def r_inline_plot(width=600, height=600, dpi=100):

    with grdevices.render_to_bytesio(grdevices.png, 
                                     width=width,
                                     height=height, 
                                     res=dpi) as b:

        yield

    data = b.getvalue()
    display(Image(data=data, format='png', embed=True))


# In[ ]:


# On utilise maintenant la fonction `plot` de `R`
# pour afficher le resultat de la recherche:
rplot = robjects.r['plot']
par = robjects.r['par']
c = robjects.r['c']

# Le contexte `r_inline_plot` permet d'afficher le
# resultat dans le notebook.
with r_inline_plot():
    par(mfrow=c(2,2))
    rplot(recherche, scale='bic')
    rplot(recherche, scale="r2")
    rplot(reg_summary['bic'], ylab="BIC", type="l")
    rplot(reg_summary['rsq'], ylab="R2", type="l")


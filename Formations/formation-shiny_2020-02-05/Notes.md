# Formation shiny avancé 05/02/2020

Option Cluster leaflet -> option leaflet pour regrouper les points en fonction du zoom

Proxy leaflet pour recharger uniquement les points leaflet et non toute la carte (avec les tuiles !)

```
leafletProxy %>% clearMarker %>% addMarker
```

Faire un schéma des intéractions réactives pour voir si il faut mieux organiser

reactiveValues -> une liste de résultat donc le calcul dépend de plusieurs actions possibles

debug: reactiveLog, browser, show code during run
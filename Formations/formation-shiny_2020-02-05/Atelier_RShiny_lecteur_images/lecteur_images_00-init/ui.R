
# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Exercice proposé par Sylvain Falala (INRA UMR ASTRE Montpellier)
# Application lecteur_images
# V00_init


# Fichier de définition de l'interface utilisateur pour l'application Shiny


library(shiny)


# Le package DT est une implémentation dans R de la librairie JavaScript DataTables
# Matrices et data frames peuvent être affichés sous forme de tableaux sur des pages HTML
# DataTables permet notamment le filtrage, la pagination et le tri de ces tableaux.
# https://rstudio.github.io/DT/

library(DT)

library(shinyjs)


ui <- fluidPage(
  useShinyjs(),
  titlePanel("Lecteur d'images"),
  sidebarLayout(
    sidebarPanel(
      # Pour plus d'info sur fileInput :
      # https://shiny.rstudio.com/reference/shiny/1.4.0/fileInput.html
      fileInput(
        inputId = "fiImage",
        label = "Sélectionnez les images",
        multiple = TRUE,
        accept = c(".gif", ".jpg", ".jpeg", ".png"),
        width = NULL,
        buttonLabel = "Parcourir...",
        placeholder = "Pas de fichier"
      ),
      # output de type DataTable
      DTOutput(outputId = "dtImageTable"),
      hidden(actionButton(inputId = "prevImg", label = "Précédant")),
      hidden(actionButton(inputId = "nextImg", label = "Suivant")),
      hidden(actionButton(inputId = "play", label = "Lecture"))
    ),
    mainPanel(
      # Affichage des images
      imageOutput(outputId = "ioImageDisplay")
    )
  )
)

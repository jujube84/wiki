library(shiny)

ui <- fluidPage(
  numericInput("number1", label = "", value = 0),
  numericInput("number2", label = "", value = 0),
  actionButton("addButton", "Add"),
  actionButton("subButton", "Sub"),
  verbatimTextOutput("res")
)

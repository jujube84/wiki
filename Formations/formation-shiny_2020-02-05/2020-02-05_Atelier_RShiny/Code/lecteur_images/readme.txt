Atelier R Shiny 5 février 2020
INRAE Avignon
Formateurs : 
Sylvain Falala (INRAE UMR ASTRE Montpellier)
Guillaume Cornu (CIRAD UR Forêts et Sociétés Montpellier)

Application lecteur_images
L'application permet de charger et afficher des fichiers d'images (jpg, png,...)
Le développement de cette application permet d'étudier plusieurs fonctions de réactivité : 
reactive
observe
eventReactive
observeEvent
reactiveValues
isolate
invalidateLater


Consignes :

1) Charger plusieurs fichiers images avec un fileInput et lister les noms de fichiers dans un DataTable du package DT
2) Afficher l’image lorsqu’on sélectionne un nom de fichier en cliquant dans le DataTable
3) Programmer un bouton "Suivant" qui affiche l’image suivante à chaque fois que le bouton est cliqué
4) Programmer un bouton "Lecture" de défilement automatique des images


# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Exercice proposé par Sylvain Falala (INRAE UMR ASTRE Montpellier)
# Application maladies
# V09_dashboard

# Fichier de définition de l'interface utilisateur pour l'application Shiny

#### Définition de l'en-tête ####

header <- dashboardHeader(title = "Application PPA")


#### Définition de la barre latérale ####


sidebar <- dashboardSidebar(
              
  # Cases à cocher avec les noms des pays définis dans global.R
  
  checkboxGroupInput(inputId = "cbCountry", 
                     label = "Choisissez le pays",
                     choices = countries, # countries = noms des pays définis dans global.R
                     selected = countries # tous les pays sont sélectionnés
                    ),
  
  
  # Slider temporel avec double curseur pour définir une période
  # min et max doivent être des dates pour que le slider soit temporel et défile par jour
  # L'option value doit avoir un vecteur de 2 dates pour disposer du double curseur
  
  sliderInput(inputId = "sliderPeriod", 
              label = "Choisissez la période", 
              min = minDate, # minDate définie dans global.R
              max = maxDate, # maxDate définie dans global.R
              value = c(minDate, maxDate)
              ),
  
  # Menu avec 3 onglets :
  # - Informations : affiche une page avec 3 boites : histogramme, tableau et résumé
  # - Carte : affiche la carte leaflet
  # - A propos de : affiche le texte html sur l'auteur
  
  sidebarMenu(
  
    menuItem(text = "Informations", tabName = "infoTab"),
    
    menuItem(text = "Carte", tabName = "mapTab"),
    
    menuItem(text = "A propos de", tabName = "aboutTab")
              
  )
)



#### Définition du corps ####

body <- dashboardBody(
  
  # Pour faire correspondre un menuItem avec un tabItem,
  # assurez-vous qu'ils ont des valeurs correspondantes pour tabName
  
  tabItems(
    
    # Page Informations avec 3 boites : histogramme, tableau et résumé
    
    tabItem(tabName = "infoTab", 
            
            # On va agencer les boites sur 1 ligne avec 2 colonnes
            # La première colonne contient l'histogramme et en dessous, le résumé statistique
            # La deuxième colonne contient le tableau
            
            fluidRow(
              
              column(width = 6, # largeur que prend la colonne sur la ligne, sur une échelle de 12
              
                # Définition d'une boite
                     
                box(title = "Histogramme nb cas/mois", # en-tête titre 
                    status = "primary", # couleur pour l'en-tête et le cadre de la boite, ici bleu foncé
                    solidHeader = TRUE, # colorie l'en-tête et le cadre avec la couleur status
                    width = NULL, # prend toute la largeur de la colonne
                    
                    # Affichage de l'histogramme du nombre de cas par mois
                    
                    plotOutput(outputId = "plotHisto")
                    
                ),
                
                
                box(title = "Statistiques", 
                    status = "warning", # couleur orange
                    solidHeader = TRUE,
                    width = NULL, 
                    
                    # Texte pour le résumé statistique
                    
                    verbatimTextOutput(outputId = "verbTextSummary")
                    
                )
              ),
              
              column(width = 6,
                     
                 box(title = "Tableau nb cas/zone administrative", 
                     status = "danger", # couleur rouge
                     solidHeader = TRUE,
                     width = NULL, 
                     
                     # Tableau du nombre de cas par zone administrative
                     
                     DTOutput(outputId = "tableAdmin")
                     
                 )
                     
               )
              
            )
            
    ),

    
    # Page avec la carte leaflet
    
    tabItem(tabName = "mapTab",
            
            leafletOutput(outputId = "outbreakMap", height = 800)
            
            ),
    
    
    # Page avec les informations sur l'auteur
    
    tabItem(tabName = "aboutTab",
            
            uiOutput(outputId = "uiAbout")
            
            )
    
    
  )
  
)


#### Définition de la page principale ####

dashboardPage(
  header,
  sidebar,
  body
)



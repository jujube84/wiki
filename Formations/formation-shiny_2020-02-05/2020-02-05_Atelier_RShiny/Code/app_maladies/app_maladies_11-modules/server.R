
# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Exercice proposé par Sylvain Falala (INRAE UMR ASTRE Montpellier)
# Application maladies
# V11_modules

# Fichier pour gérer les interactions de l'application Shiny


# Paramètres d'entrée :
# input : liste de boutons, menus déroulants, curseurs... définis dans UI
# output : liste des affichages (tableaux, graphes, cartes...) définis dans UI


server <- function(input, output) {
  
  #### Afrique ####
  
  callModule(mod_selecmap_server, # nom de la fonction server du module
             
             id = "africa", # Attention à bien donner le même id que dans ui !
             
             regionDF = AfricaDF) # callModule envoie automatiquement à la fonction server du module 
                                  # les paramètres input, output, session puis les paramètres suivants, ici regionDF
  
  
  #### Asie ####
  
  callModule(mod_selecmap_server, id = "asia", regionDF = AsiaDF)
  
  
  #### Europe ####
  
  callModule(mod_selecmap_server, id = "europe", regionDF = EuropeDF)
  
}



# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Exercice proposé par Sylvain Falala (INRAE UMR ASTRE Montpellier)
# Application maladies
# V05_slider

# Fichier de définition de l'interface utilisateur pour l'application Shiny

ui <- fluidPage(
  
  #### Panneau avec le titre de l'application ####
  
  titlePanel("Application PPA"),
  
  # Mise en page avec un panneau barre latérale et un panneau principal
  
  sidebarLayout(
    
    #### Panneau barre latérale ####
    
    sidebarPanel(
      
      # Cases à cocher avec les noms des pays définis dans global.R
      
      checkboxGroupInput(inputId = "cbCountry", 
                         label = "Choisissez le pays",
                         choices = countries, # countries = noms des pays définis dans global.R
                         selected = countries # tous les pays sont sélectionnés
      ),
      
      
      # Slider temporel avec double curseur pour définir une période
      # min et max doivent être des dates pour que le slider soit temporel et défile par jour
      # L'option value doit avoir un vecteur de 2 dates pour disposer du double curseur
      
      sliderInput(inputId = "sliderPeriod", 
                  label = "Choisissez la période", 
                  min = minDate, # minDate définie dans global.R
                  max = maxDate, # maxDate définie dans global.R
                  value = c(minDate, maxDate)
      )
      
      
    ),
    
    #### Panneau principal ####
    
    mainPanel(
      
      # Groupe d'onglets
      
      tabsetPanel(
        
        tabPanel("Histo mois", plotOutput(outputId = "plotHisto")), # Histogramme du nombre de cas par mois
        
        tabPanel("Tableau admin", DTOutput(outputId = "tableAdmin")), # Tableau du nombre de cas par zone admin
        
        tabPanel("Résumé", verbatimTextOutput(outputId = "verbTextSummary")), # Résumé statistique
        
        tabPanel("A propos", uiOutput(outputId = "uiAbout")) # Informations sur l'auteur
      )
    )
  )
  
)

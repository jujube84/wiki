Atelier R Shiny 5 février 2020
INRAE Avignon
Formateurs : 
Sylvain Falala (INRAE UMR ASTRE Montpellier)
Guillaume Cornu (CIRAD UR Forêts et Sociétés Montpellier)

Application maladies

Objectif : construire une application pour afficher des données épidémiologiques

Données traitées : fichier csv obtenu sur Empres-i : http://empres-i.fao.org/eipws3g/

Consignes :

Charger le fichier csv dans global.R. Le fichier est dans le sous-dossier "data"

Créer l’interface avec comme éléments : 

Input : liste de cases à cocher avec tous les noms de pays

Output : un onglet pour chaque output : 

	Histogramme du nombre de cas (lignes) par mois
	Tableau avec le nombre de cas par zone administrative
	Résumé statistique des sumCases, sumDeaths et sumDestroyed
	Onglet « à propos » : nom du programmeur, lien vers Empres-i et nombre de lignes traitées

Deuxième input : slider avec 2 boutons pour sélectionner une période en jours

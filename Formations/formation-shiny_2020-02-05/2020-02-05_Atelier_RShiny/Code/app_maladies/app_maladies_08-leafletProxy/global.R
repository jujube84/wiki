
# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Exercice proposé par Sylvain Falala (INRAE UMR ASTRE Montpellier)
# Application maladies
# V08_leafletProxy


# global.R se charge avant ui.R et server.R
# Il permet de partager des variables entre ui et server : les variables sont dans l'environnement global
# Il permet par exemple de paramétrer les inputs dans ui en fonction de données lues dans un fichier


#### Chargement des librairies ####

library(shiny)

# Le package ggplot2 permet de créer des graphiques
# https://ggplot2.tidyverse.org/reference/index.html

library(ggplot2)

# Le package dplyr permet la manipulation de données
# On l'utilise ici pour la fonction count
# https://dplyr.tidyverse.org/reference/index.html

library(dplyr)

# Le package DT est une implémentation dans R de la librairie JavaScript DataTables
# Matrices et data frames peuvent être affichés sous forme de tableaux sur des pages HTML
# DataTables permet notamment le filtrage, la pagination et le tri de ces tableaux. 
# https://rstudio.github.io/DT/

library(DT)

# Le package leaflet est une implémentation dans R de la librairie JavaScript leaflet
# Il permet de créer des cartes interactives
# https://rstudio.github.io/leaflet/

library(leaflet)


#### Chargement du fichier de données ####

# Fichier de données Empres-i sur les foyers de Peste Porcine Africaine (African Swine Fever en anglais)
# en Europe du 1er janvier 2018 au 30 septembre 2019
# Obtenu librement sur le site Empres-i : http://empres-i.fao.org/eipws3g/

asfDF <- read.csv(file = "data/Empres-i_ASF_20180101_20190930.csv", 
                  sep = ";", 
                  dec = ",", 
                  header = TRUE, 
                  row.names = NULL, 
                  stringsAsFactors = FALSE)

# Pour connaitre les noms de colonnes et leur type
#print(str(asfDF))


#### Conversion de la date d'observation au format Date ####

# Colonne "observationDate"
# A l'origine au format character
# Ce format est précisé dans as.Date avec les options %Y = année, %m = mois, %d = jour
asfDF$observationDate <- as.Date(asfDF$observationDate, "%Y-%m-%d")
asfDF$reportingDate <- as.Date(asfDF$reportingDate, "%Y-%m-%d")


#### Extraction des mois à partir de la date d'observation ####

# Colonne "observationDate"
# %m renvoie le numéro du mois sur 2 chiffres
# %Y renvoie l'année sur 4 chiffres
# cf aide de strftime
asfDF$month <- strftime(asfDF$observationDate, "%Y %m")


#### Récupération des noms de pays ####

# Colonne "country"
# Les noms de pays vont servir à paramétrer les cases à cocher dans ui
countries <- sort(unique(asfDF$country))


#### Récupération des dates d'observation min et max pour le slider temporel ####

minDate <- min(asfDF$observationDate)
maxDate <- max(asfDF$observationDate)


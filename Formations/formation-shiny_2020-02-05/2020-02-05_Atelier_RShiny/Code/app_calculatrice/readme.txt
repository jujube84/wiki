Atelier R Shiny 5 février 2020
INRAE Avignon
Formateurs : 
Sylvain Falala (INRAE UMR ASTRE Montpellier)
Guillaume Cornu (CIRAD UR Forêts et Sociétés Montpellier)

Application calculatrice

Version 1 : 

Calculatrice avec 2 entrées numériques
1 sortie type console pour afficher le résultat
Calcule la somme et affiche le résultat dès que la valeur d'une entrée numérique change
Utilisation basique de Shiny : inputs observés par un render


Version 2 : 

Calculatrice avec 2 entrées numériques
1 bouton + pour additionner les 2 valeurs
1 sortie type console pour afficher le résultat
Le résultat n'est affiché que lorsqu'on clique sur le bouton +
Fonction intéressante : isolate


Version 3 : 

Identique à la version 2 mais utilise la fonction eventReactive


Version 4 :

Calculatrice avec 2 entrées numériques
1 bouton + pour additionner les 2 valeurs
1 bouton - pour soustraire les 2 valeurs
1 sortie type console pour afficher le résultat
Le résultat n'est affiché que lorsqu'on clique sur un des deux boutons
Fonctions intéressantes : reactiveValues, observeEvent


Version 5 :

Semblable version 4
Calculatrice avec 2 entrées numériques
1 bouton + pour additionner les 2 valeurs
1 bouton - pour soustraire les 2 valeurs
1 bouton -- pour soustraire les 2 valeurs, qui sert pour tester le observeEvent avec plusieurs événements 
1 sortie type console pour afficher le résultat
Le résultat n'est affiché que lorsqu'on clique sur un des trois boutons
Fonction intéressante : observeEvent sur une liste d’inputs


# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Application calculatrice
# Par Sylvain Falala (INRAE UMR ASTRE Montpellier)
# et Guillaume Cornu (CIRAD UR F&S Montpellier)
# app_calculatrice_04-plusmoins-reactiveValues

# Calculatrice avec 2 entrées numériques
# 1 bouton + pour additionner les 2 valeurs
# 1 bouton - pour soustraire les 2 valeurs
# 1 sortie type console pour afficher le résultat
# Le résultat n'est affiché que lorsqu'on clique sur un des deux boutons

# Fichier de définition de l'interface utilisateur pour l'application Shiny

library(shiny)

ui <- fluidPage(
  
  #### Première entrée numérique ####
  
  numericInput(inputId = "numA", label = NULL, value = 1),
  
  #### Deuxième entrée numérique ####
  
  numericInput(inputId = "numB", label = NULL, value = 1),
  
  #### Bouton d'addition ####
  
  actionButton(inputId = "abPlus", label = "+"),
  
  #### Bouton de soustraction ####
  
  actionButton(inputId = "abMinus", label = "-"),
  
  #### Affichage du résultat ####
  
  verbatimTextOutput(outputId = "textResult")
  
)



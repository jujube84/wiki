
# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Application de démonstration pour les diapositives de cours
# Par Sylvain Falala (INRAE UMR ASTRE Montpellier)
# mon_app_shiny_03-sidebarLayout

# Fichier de définition de l'interface utilisateur pour l'application Shiny

library(shiny)

ui <- fluidPage(
  
  #### Panneau avec le titre de l'application ####
  
  titlePanel("Mon application Shiny !"),
  
  
  # Mise en page avec un panneau barre latérale et un panneau principal
  
  sidebarLayout(
    
    #### Panneau barre latérale ####
    
    sidebarPanel(
      
      p(strong("Et si je mettais mes contrôles ici ?")),
      
      p("Contrôle 1"),
      
      p("Contrôle 2")
      
    ),
    
    #### Panneau principal ####
    
    mainPanel(
      
      h1("Et je vais mettre ici mes graphiques !"),
      
      img(src = "logo_shiny.png")
      
    )
  )
  
)

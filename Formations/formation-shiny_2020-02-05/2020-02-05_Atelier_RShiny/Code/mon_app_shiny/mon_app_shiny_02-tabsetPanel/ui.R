
# Atelier R Shiny 5 février 2020
# INRAE Avignon
# Application de démonstration pour les diapositives de cours
# Par Sylvain Falala (INRAE UMR ASTRE Montpellier)
# mon_app_shiny_02-tabsetPanel

# Fichier de définition de l'interface utilisateur pour l'application Shiny

library(shiny)


ui <- fluidPage(
  
  #### Panneau avec le titre de l'application ####
  
  # La police du titre est automatiquement grande
  # C'est aussi le titre de la fenêtre du navigateur
  titlePanel("Mon application Shiny !"),

  
  #### Groupe d'onglets ####
  
  tabsetPanel(
  # Au lieu d'avoir des onglets, il est possible d'avoir un menu de sélection vertical sur la gauche 
  # avec navlistPanel à la place de tabsetPanel 
    
    # Premier onglet
    
    tabPanel("Shiny", 
             
             h1("Je code avec les fonctions Shiny"),
             
             img(src = "logo_shiny.png"),
             
             p(strong("Gras avec une fonction Shiny")),
             p(em("Italique avec une fonction Shiny")),
             p(code("Code avec une fonction Shiny")),
             a(href="https://www.cirad.fr", target = "_blank", "Lien avec une fonction Shiny")
             
             ),
    
    # Deuxième onglet
    
    tabPanel("HTML5", 
             
             HTML('<h1>Je code avec les balises HTML5</h1>
                   <img src="logo_HTML5.png"/>
                   <p>
                      <strong>Gras avec une balise HTML5</strong>
                   </p>
                   <p>
                      <em>Italique avec une balise HTML5</em>
                   </p>
                   <p>
                      <code>Code avec une balise HTML5</code>
                   </p>
                   <a href="https://www.cirad.fr" target="_blank">Lien avec une balise HTML5</a>
                  ')
             )
      )
  
)

#!/bin/bash

# This script build a book created in RMarkDown

old_dir=$PWD

mkdir /tmp/book
cd /tmp/book

wget -c "$1/archive/master.zip"
unzip master.zip
rm master.zip
cd `ls`

# We are using grep to catch all the packages the book need to be build
# -P: use Perl regex because of the lookahead and lookbehind
# -r: recursive file search
# -o: only print what matched
# -h: don't print the files from where match come
# --include \*.Rmd: search only in .Rmd files
# Then we use sort to remove duplicates
# -u: only keep one occurance of each words
# Convert the list of packages to an R vector c("...", "...")
cmd="c("
for pkg in $(grep -Proh -e "(?<=library\()\w+|\w+(?=::)" --include \*.Rmd | sort -u); do
    cmd="${cmd}\"${pkg}\"," 
done
cmd="${cmd%?})" # Remove the last ","

# Only install the packages not installed
Rscript -e "pkgs <- ${cmd}; \
            new_pkgs <- pkgs[!(pkgs %in% installed.packages()[,\"Package\"])]; \
            print(paste(\"Packages that need to be installed:\", paste(new_pkgs, collapse = \"/\"))); \
            if(length(new_pkgs)) install.packages(new_pkgs)"

# Build the book in PDF format
Rscript -e "bookdown::render_book(\"index.Rmd\", \
            output_format = \"bookdown::pdf_book\", \
            new_session = TRUE)"

# rm -R /tmp/$1
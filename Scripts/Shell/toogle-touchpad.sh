#!/bin/bash
# Lock/unlock the touchpad (Work with xfce4) 
if [ $(synclient -l | grep TouchpadOff | cut -d "=" -f2) -eq 1 ]; then {
    synclient TouchpadOff=0;
} else {
    synclient TouchpadOff=1;
} fi

## *Each cmd need to be run as sudo (`sudo su`)*

# Install Docker (Ubuntu)
	apt-get install \
	  apt-transport-https \
	  ca-certificates \
	  curl \
	  software-properties-common

	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

	add-apt-repository \
	  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
	  $(lsb_release -cs) \
	  stable"

	apt-get update
	apt-get install docker-ce

# Login
	docker login --username USER HOST:PORT

# Pull
	docker pull HOST:PORT/PATH:TAG

# Build
	docker build DOCKERFILEPATH --tag NAME:TAG

# Run 
	docker run --rm -p XXXX:XXXX -it HOST:PORT/PATH:TAG
	docker run --rm -p XXXX:XXXX -it HOST:PORT/PATH:TAG /bin/bash # for a shell access

# Connect to a running container
	docker exec -it HOST:PORT/PATH:TAG /bin/bash

# List containers
	docker ps # running
	docker ps -a # all

# Remove container
	docker rm CONTAINERID # or name 
	docker rm $(docker ps -a -q) # all

# List images
	docker images

# Remove image
## You need to delete the related containers before, unless you use `--force`
	docker rmi IMAGEID
	docker rmi $(docker images -q) # all
	docker rmi $(docker images -q) --force # Remove also the containers using the images deleted

# INSERT
    INSERT INTO table VALUES (val1, ...);
    INSERT INTO table (col1, ...) VALUES (val1, ...);
    INSERT INTO table VALUES (val1, ...),
                            (val2, ...);

# UPDATE
    UPDATE table SET col = val, ... WHERE ...;

# DELETE
    DELETE FROM table WHERE ...;

# Find duplicate value
    SELECT * FROM table WHERE col IN (
        SELECT col FROM table
        GROUP BY col HAVING COUNT(1) > 1
    )
Général
=======
Texte centré
------------
	\centerline{\url{https://www.overleaf.com/signup?ref=d62cb1694be6}}
Mise en forme de code
---------------------
	\begin{lstlisting}[language=TeX]
		\documentclass{ceri}
	\end{lstlisting}
Abréviation énième
------------------
	2\ieme, 3\ieme
Espace insécable
----------------
	~
Accent dans un document en anglais
----------------------------------
	\'Et\'e (Été)
	biblioth\`eque (bibliothèque)
Algorithme (pseudo-code)
-----------------------
	\begin{algorithm}[htb!]
		\KwData{$x, y$} % Variables d'entrée
		\KwResult{M} % Variables de sortie

		\BlankLine % Saut de ligne
		\tcp{Initialisation} % Commentaire
		$z \leftarrow 50$\;

		\BlankLine
		\tcp{Boucle principale}
		\For{$i \leftarrow 1$ to $x$}{
			\While{$y \leq 18$}{
				\For{$u \in V$}{
					\If{$u \neq $v}{ 
						$a  \leftarrow a + 1$\; \label{line:exemple} % Référence avec \ref{line:exemple}
						$y \leftarrow x + y / 2$\;
					}
					\eIf{$x > y$}{
						$x \leftarrow x \times y$\;
					}{
						$y \leftarrow \sqrt{x + y / 2}$\;
					}
				}
			}
			
			\BlankLine
			\tcp{Un autre commentaire}
				$B \leftarrow (x + y)^a$\;
		}
		$M \leftarrow ||B||$\;

		\caption{Exemple d'algorithme bidon, représenté sous forme de pseudo-code.}
		\label{algo:exemple}
	\end{algorithm}
Section/sous-section non numéroté
--------------------
	\section*{...}
	\subsection*{...}

Référence
=========
Faire référence à une section / sous section
--------------------------------------------
	\ref{sec:Introduction}
Citation biblio
---------------
	\cite{LaTeXProject2010, Wikipedia2011a}
Note bas de page
----------------
	\footnote{La numérotation se fait automatiquement.}
Caption
-------
	\caption[short]{long} % short sera utilisé pour la liste des figures/tableaux, et long sera affiché sous l'image/tableau

Image/tableau
=============
Insertion image matricielle
---------------------------
	\begin{figure}[htb!]
		\centering
		\includegraphics[scale=0.5]{images/univ.jpg}
		\caption[Université d'Avignon]{La façade de l'Université d'Avignon.}
		\label{fig:uapv}
	\end{figure}
Insertion image vectorielle
---------------------------
	\begin{figure}[htb!]
		\centering
		\resizebox{0.75\linewidth}{!}{\input{images/classdiag.tex}}
		\caption[Université d'Avignon]{Exemple de diagramme.}
		\label{fig:diag}
	\end{figure}
Insertion tableau
-----------------
	\begin{table}[htb!]
		\centering
		\rowcolors{1}{LightColor}{}
		\begin{tabular}{l c r r}
			\hline
			\rowcolor{DarkColor} 
			\textbf{Texte} & \textbf{Texte} & \textbf{Entiers} & \textbf{Réels}	\\ 
			\hline
			Blabla bla bla bla bla & Blabla bbla bla & 21 & 12,62 \\
			Blabla bla bla bla & Blabla bla bla bla bla & 12356 & 3456,21 \\
			Blabla bla bla & Blabla bla bbla bla & 1 & 45,87 \\
			\hline
		\end{tabular}
		\caption[Exemple de table hétérogène]{Exemple de table contenant du texte, des valeurs entières, réelles et des formules mathématiques.}
		\label{tab:exemple}
	\end{table}
Multi tableau 
-------------
	\begin{table}[htb!]
		\centering
		\rowcolors{1}{LightColor}{}
		% Première partie de la page...
		\begin{minipage}[t]{0.19\linewidth}
			\centering
			\begin{tabular}{l l}
				\hline
				\rowcolor{DarkColor} 
				\textbf{Col1} & \textbf{Col2} \\ 
				\hline
				1 & hdc \\
				2 & orf \\
				3 & seh \\
				4 & lig \\
				5 & jhg \\
				6 & azr \\
				7 & opi \\
				8 & hgj \\
				9 & xvc \\
				10 & ree \\
				\hline
			\end{tabular}
		\end{minipage}
		% Deuxième partie de la table...
		\begin{minipage}[t]{0.19\linewidth}
			\centering
			\begin{tabular}{l l}
				\hline
				\rowcolor{DarkColor} 
				\textbf{Col1} & \textbf{Col2} \\ 
				\hline
				11 & ytr \\
				12 & xcv \\
				13 & fsd \\
				14 & pui \\
				15 & gsf \\
				16 & aze \\
				17 & bcv \\
				18 & wxc \\
				19 & uyr \\
				20 & ndz \\
				\hline
			\end{tabular}
		\end{minipage}
		\caption[Exemple de longue table]{Exemple de table haute et étroite, décomposée en 2 parties pour occuper plus efficacement l'espace disponible.}
		\label{tab:etroit}
	\end{table}

Math
====
Symbole multiplication
----------------------
	$7 \times 2 = 14$
Équation hors ligne
-------------------
	\begin{equation}
		f(s) = h(s) + \max(g_1(s), g_2(s))
		\label{eq:exemple}
	\end{equation}

Tikz
====

Beamer
=====
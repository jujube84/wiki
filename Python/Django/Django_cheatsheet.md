# Create a new project
    django-admin startproject NAME

# Run a local server
    python manage.py runserver

# Migration (Create and modify the DB tables so they can store the models data of the modules)
```bash
    # Prepare the migration of a module
    python manage.py makemigrations MODULE
    # Display the SQL code generate by a migration
    python manage.py sqlmigrate MODULE MIGRATION_ID
    # Apply the migration
    python manage.py migrate
```

# Inspect and check the project for common problems
    python manage.py check

# Django interactive Python shell
    python manage.py shell

# Add a admin account
    python manage.py createsuperuser

# Database API
## Model
```python
# Import a model
from MODULE.models import MODEL
# Display all the instances of a model (Need a `__str__` method for the model)
MODEL.objects.all()
# Create an instance of a model
m = MODEL(field1="text", field2=0)
# Save the instance to the DB
m.save()
# Change localy the value of a field
m.field2 = 1
# Apply the change
m.save()
# Filter the instances
MODEL.objects.filter(field1__startwith="t") # equivalent to field1.startwith("t")
MODEL.objects.filter(field2=1)
# Get one instance
m = MODEL.objects.get(field2=1)
m = MODEL.objects.get(pk=1) # Using the primary key
# Get the instances of another model related to this model by a foreign key
m.FK_MODEL_set.all()
# Create a instance of the other model and link it to the model instance
m2 = m.FK_MODEL_set.create(field1="2020-05-07")
m2.FK_FIELD # Acces to m
# Filter the instances of the other model using a field from the model instance
FK_MODEL.objects.filter(FK_FIELD__field2=1)
# Delete an instance
m2.delete()
```